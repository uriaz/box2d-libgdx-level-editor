package com.minibit.gameUtils;

import java.util.ArrayList;

import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.minibit.gameUtils.LevelEditorShape.ShapeType;

public class BodyDescriptor
{
	private String layerId, resourcePath;
	private ArrayList<String> fixtureIds = new ArrayList<String>();
	private String bodyId;
	private BodyType bodyType;
	private ShapeType shapeType;
	private boolean mapBody = false;
	private boolean subDestructionBody = false;
	private int groupIndex = 0;
	private ArrayList<Tuple<String, String>> extraProperties = new ArrayList<Tuple<String, String>>();

	private final static String KINEMATIC_BODY = "kinematic";
	private final static String STATIC_BODY = "static";
	private final static String DYNAMIC_BODY = "dynamic";

	public BodyDescriptor(String layerId, String startBodyId, String resourcePath)
	{
		this.layerId = layerId;
		this.resourcePath = resourcePath;
		this.bodyId = startBodyId;
	}

	public BodyDescriptor(String layerId, String startBodyId, String resourcePath, ShapeType shapeType)
	{
		this.layerId = layerId;
		this.resourcePath = resourcePath;
		this.bodyId = startBodyId;
		this.shapeType = shapeType;
	}

	public BodyDescriptor(BodyDescriptor bodyDescriptor)
	{
		this.layerId = bodyDescriptor.layerId;
		this.resourcePath = bodyDescriptor.resourcePath;
		this.fixtureIds = bodyDescriptor.fixtureIds;
	}

	@Override
	public String toString()
	{
		return bodyId + ", " + layerId + ", " + resourcePath;
	}

	public String getLayerId()
	{
		return layerId;
	}

	public void setLayerId(String layerId)
	{
		this.layerId = layerId;
	}

	public String getResourcePath()
	{
		return resourcePath;
	}

	public void setResourcePath(String resourcePath)
	{
		this.resourcePath = resourcePath;
	}

	public ArrayList<String> getFixtureIds()
	{
		return fixtureIds;
	}

	public void setFixtureIds(ArrayList<String> fixtureIds)
	{
		this.fixtureIds = fixtureIds;
	}

	public String getBodyId()
	{
		return bodyId;
	}

	public void setBodyId(String bodyId)
	{
		this.bodyId = bodyId;
	}

	public BodyType getBodyType()
	{
		return bodyType;
	}

	public void setBodyType(String bodyType)
	{
		if (bodyType != null)
		{
			if (bodyType.equals(KINEMATIC_BODY))
			{
				this.bodyType = BodyType.KinematicBody;
			}
			else if (bodyType.equals(DYNAMIC_BODY))
			{
				this.bodyType = BodyType.DynamicBody;
			}
			else if (bodyType.equals(STATIC_BODY))
			{
				this.bodyType = BodyType.StaticBody;
			}
		}
	}

	public ShapeType getShapeType()
	{
		return shapeType;
	}

	public void setShapeType(ShapeType shapeType)
	{
		this.shapeType = shapeType;
	}

	public boolean isMapBody()
	{
		return mapBody;
	}

	public void setMapBody(boolean mapBody)
	{
		this.mapBody = mapBody;
	}

	public ArrayList<Tuple<String, String>> getExtraProperties()
	{
		return extraProperties;
	}

	public void setExtraProperties(ArrayList<Tuple<String, String>> extraProperties)
	{
		this.extraProperties = extraProperties;
		for (int i = 0; i < extraProperties.size(); i++)
		{
			if (extraProperties.get(i).getKey().equals("groupIndex"))
			{
				groupIndex = Integer.parseInt(extraProperties.get(i).getValue());
			}
		}
	}

	public boolean isSubDestructionBody()
	{
		return subDestructionBody;
	}

	public void setSubDestructionBody(boolean subDestructionBody)
	{
		this.subDestructionBody = subDestructionBody;
	}

	public int getGroupIndex()
	{
		return groupIndex;
	}

	public void setGroupIndex(int groupIndex)
	{
		this.groupIndex = groupIndex;
	}
}
