package com.minibit.gameUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;

import com.minibit.sprites.PhysicsSprite;

public class ActivationTriggerHolder
{
	private float activationX = NO_TRIGGER, activationY = NO_TRIGGER, angleActivation = NO_TRIGGER, velocityActivation = NO_TRIGGER;

	// Getters for these methods must follow the pattern of 'getX()' or 'getAngle()'
	public final static String X_KEY = "x", Y_KEY = "y";
	private final static String[] KEYS =
	{ X_KEY, Y_KEY };

	public final static int NO_TRIGGER = -63633453; // Random number to signify no trigger

	private HashMap<String, Float> activationMap;
	private HashMap<String, Method> getterMap;

	public ActivationTriggerHolder()
	{
		activationMap = new HashMap<String, Float>();
		getterMap = new HashMap<String, Method>();
		// Load in getter methods
		initializeGetters();
	}

	private void initializeGetters()
	{
		for (int i = 0; i < KEYS.length; i++)
		{
			String methodId = "get" + Character.toUpperCase(KEYS[i].charAt(0));
			try
			{
				Method getter = PhysicsSprite.class.getMethod(methodId);
				getterMap.put(KEYS[i], getter);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public Method getMethodForKey(String key)
	{
		return getterMap.get(key);
	}

	public void addActivation(String key, float value)
	{
		activationMap.put(key, value);
	}

	public float getActivationValue(String key)
	{
		Object value = activationMap.get(key);
		if (value != null)
		{
			float fValue = ((Float) value).floatValue();
			return fValue;
		}

		return NO_TRIGGER;
	}

	public void setActivationValue(String key, float value)
	{
		activationMap.put(key, value);
	}

	public Iterator<String> getKeys()
	{
		return activationMap.keySet().iterator();
	}
}
