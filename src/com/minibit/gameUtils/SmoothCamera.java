package com.minibit.gameUtils;

import com.badlogic.gdx.graphics.OrthographicCamera;

public class SmoothCamera extends OrthographicCamera
{

	public SmoothCamera(float viewportWidth, float viewportHeight, float diamondAngle)
	{
		super(viewportWidth, viewportHeight, diamondAngle);
	}

	public SmoothCamera(float viewportWidth, float viewportHeight)
	{
		super(viewportWidth, viewportHeight);
	}

	public SmoothCamera()
	{
		super();
	}

	private int iterationTracker = 0;
	private int zoomIterations = 0;
	private float desiredZoom = 0.0f;
	public boolean isFinishedZooming = false;

	@Override
	public void update()
	{
		super.update();
		if (zoomIterations != 0 && desiredZoom != 0.0f && !isFinishedZooming)
		{
			smoothZoom(desiredZoom, zoomIterations);
		}
	}

	// As camera zoom approaches 0, camera zooms in.
	private void smoothZoom(float desiredZoom, int iterations)
	{
		// Geometric series; Tend = desiredZoom, Tstart = currentZoom
		// Tn = t1 * r ^ n-1
		zoomIterations = iterations;
		this.desiredZoom = desiredZoom;
		float currentZoom = this.zoom;
		float step = (float) Math.pow(10, Math.log10(desiredZoom / currentZoom) / (iterations - 1));
		float possibleNextZoom = this.zoom * step;

		if (desiredZoom < currentZoom)
		{
			if (possibleNextZoom > desiredZoom && Math.abs(possibleNextZoom - desiredZoom) > 0.000001f)
			{
				this.zoom = possibleNextZoom;
			}
			else
			{
				isFinishedZooming = true;
			}
		}
		else if (desiredZoom > currentZoom)
		{
			if (possibleNextZoom < desiredZoom && Math.abs(possibleNextZoom - desiredZoom) > 0.000001f)
			{
				this.zoom = possibleNextZoom;
			}
			else
			{
				isFinishedZooming = true;
			}
		}
	}

	public void setFinishedZooming(boolean isFinished)
	{
		this.isFinishedZooming = isFinished;
	}

	public void setSmoothZoom(float desiredZoom, int iterations)
	{
		this.desiredZoom = desiredZoom;
		zoomIterations = iterations;
		this.isFinishedZooming = false;
	}

	public void setDesiredZoom(float desiredZoom)
	{
		this.desiredZoom = desiredZoom;
	}

	public void setIterations(int iterations)
	{
		zoomIterations = iterations;
	}
}
