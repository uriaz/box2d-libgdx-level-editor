package com.minibit.gameUtils;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.minibit.sprites.PointF;

public class LevelEditorShape
{
	public enum ShapeType
	{
		TYPE_POLYGON, TYPE_ELLIPSE, TYPE_CHAIN_SHAPE, TYPE_PRISMATIC_AXIS, TYPE_RECTANGLE, TYPE_JOINT
	}

	private ArrayList<PointF> worldVertices;
	private ArrayList<PointF> untranslatedVertices;
	private int[] bezierPoints; // For chain shapes / maps, when we want a smooth bezier curve between every 3 points specified. 2nd point specified
								// is the control point.
	private ShapeType selectedShape;
	private float shapeX, shapeY;
	private float rectWidth, rectHeight;
	private PointF scaleFactor = new PointF();
	private PointF editorScaleFactor = new PointF();
	private Texture spriteTexture;
	private BodyDescriptor bodyDescriptor;
	private Rectangle shapeBoundingRectangle;

	// X and Y coordinates are WORLD coordinates of the level editor; must be
	// passed in.
	public LevelEditorShape(float x, float y, BodyDescriptor bodyId)
	{
		setShapeX(x);
		setShapeY(y);
		this.shapeY = y;
		this.selectedShape = bodyId.getShapeType();
		this.bodyDescriptor = bodyId;
	}

	public LevelEditorShape(LevelEditorShape shape)
	{
		this.shapeX = shape.getShapeCenterX();
		this.shapeY = shape.getShapeCenterY();
		this.rectWidth = shape.getRectWidth();
		this.rectHeight = shape.getRectHeight();
		this.selectedShape = shape.getBodyDescriptor().getShapeType();
		this.bodyDescriptor = shape.bodyDescriptor;
		this.worldVertices = shape.worldVertices;
		this.untranslatedVertices = shape.worldVertices;
		this.bezierPoints = shape.bezierPoints;
	}

	public LevelEditorShape(Texture texture, float x, float y, BodyDescriptor bodyId)
	{
		this.spriteTexture = texture;
		setShapeX(x);
		setShapeY(y);
		this.selectedShape = bodyId.getShapeType();
		this.bodyDescriptor = bodyId;
	}

	public LevelEditorShape(PointF loc, PointF dims, PointF scaleFactor, BodyDescriptor bodyId)
	{
		this.shapeX = loc.x;
		this.shapeY = loc.y;

		if (bodyId.getShapeType() == ShapeType.TYPE_ELLIPSE)
		{
			// dims.x is equal to dims.y
			shapeX += dims.x / 2;
			shapeY -= dims.x / 2;
			setRectWidth(dims.x * scaleFactor.x);
			setRectHeight(dims.x * scaleFactor.y);
		}
		else
		{
			setRectWidth(dims.x * scaleFactor.x);
			setRectHeight(dims.y * scaleFactor.y);
		}

		this.selectedShape = bodyId.getShapeType();
		this.bodyDescriptor = bodyId;
		this.scaleFactor = scaleFactor;
	}

	public LevelEditorShape(float x, float y, ArrayList<PointF> vertices, ShapeType type, BodyDescriptor bodyId)
	{
		setShapeX(x);
		setShapeY(y);
		this.selectedShape = type;
		this.worldVertices = vertices;
		this.bodyDescriptor = bodyId;
	}

	public ShapeType getType()
	{
		return selectedShape;
	}

	public void setType(ShapeType type)
	{
		this.selectedShape = type;
	}

	public ArrayList<PointF> getWorldVertices()
	{
		return worldVertices;
	}

	/**
	 * 
	 * @param trianglePolygons
	 *            List of vertices in clockwise orientation.
	 * @param scaleFactor
	 *            The scale factor that should be applied to polygons once they have been generated. For maps, the x and y scale factors should both
	 *            be 1.0 as it would be pointless to scale a map up or down in size.
	 * @param gameScreenDimensions
	 *            The dimensions of the game screen that is visible to the player.
	 * @param editorDimensions
	 *            The dimensions of the editor that was used to create the vertices.
	 */
	public void setVertices(ArrayList<PointF> trianglePolygons, PointF scaleFactor, PointF gameScreenDimensions, PointF editorDimensions)
	{
		editorScaleFactor = new PointF(editorDimensions.x / gameScreenDimensions.x, editorDimensions.y / gameScreenDimensions.y);
		this.scaleFactor = scaleFactor;
		this.untranslatedVertices = new ArrayList<PointF>();

		float xScaleMult = scaleFactor.x;
		float yScaleMult = scaleFactor.y;

		for (int i = 0; i < trianglePolygons.size(); i++)
		{
			PointF localCoords = trianglePolygons.get(i);
			float newX = localCoords.x + this.shapeX;
			float scaledX = newX / editorDimensions.x * gameScreenDimensions.x;
			scaledX = scaledX * xScaleMult;

			float newY = localCoords.y + this.shapeY;
			float scaledY = gameScreenDimensions.y - newY / editorDimensions.y * gameScreenDimensions.y;
			scaledY = scaledY * yScaleMult;
			localCoords.set(scaledX, scaledY);

			untranslatedVertices.add(new PointF(scaledX, scaledY));
		}

		this.worldVertices = trianglePolygons;
	}

	public void createAndSetRectangleVertices(PointF dims)
	{
		// Generate rectangle points in clockwise order
		float x = shapeX;
		float y = shapeY;
		float endX = x + dims.x;
		float endY = y + dims.y;

		ArrayList<PointF> rectPoints = new ArrayList<PointF>();
		// Triangle 1
		rectPoints.add(new PointF(x, y));
		rectPoints.add(new PointF(endX, y));
		rectPoints.add(new PointF(endX, endY));

		// Triangle 2
		rectPoints.add(new PointF(x, y));
		rectPoints.add(new PointF(endX, endY));
		rectPoints.add(new PointF(x, endY));

		this.untranslatedVertices = this.worldVertices = rectPoints;

	}

	public void translatePolygon(PointF desiredLoc)
	{
		float scx = getShapeCenterX();
		float scy = getShapeCenterY();
		float dx = scx - desiredLoc.x;
		float dy = scy - desiredLoc.y;

		for (int i = 0; i < worldVertices.size(); i++)
		{
			PointF untranslatedCoords = worldVertices.get(i);
			float newX = untranslatedCoords.x - dx;
			float newY = untranslatedCoords.y - dy;
			untranslatedCoords.set(newX, newY);
		}
	}

	public float getShapeCenterX()
	{
		float scaledX = shapeX;
		scaledX *= scaleFactor.x;
		if (selectedShape == ShapeType.TYPE_RECTANGLE)
		{
			return shapeX + rectWidth / 2;
		}
		else if (selectedShape == ShapeType.TYPE_ELLIPSE || getWorldVertices() == null || getWorldVertices().size() == 0)
		{
			return scaledX;
		}
		else
		{
			return getCentroid(getWorldVertices()).x;
		}
	}

	public float getShapeCenterY()
	{
		float scaledY = shapeY;
		scaledY *= scaleFactor.y;
		if (selectedShape == ShapeType.TYPE_RECTANGLE)
		{
			return shapeY + rectHeight / 2;
		}
		else if (selectedShape == ShapeType.TYPE_ELLIPSE || getWorldVertices() == null || getWorldVertices().size() == 0)
		{
			return scaledY;
		}
		else
		{
			return getCentroid(getWorldVertices()).y;
		}
	}

	public float getUntranslatedShapeCenterX()
	{
		float scaledX = shapeX;
		scaledX *= scaleFactor.x;
		if (selectedShape == ShapeType.TYPE_RECTANGLE)
		{
			return shapeX + rectWidth / 2;
		}
		else if (selectedShape == ShapeType.TYPE_ELLIPSE || getWorldVertices() == null || getWorldVertices().size() == 0)
		{
			return scaledX;
		}
		else
		{
			return getCentroid(untranslatedVertices).x;
		}
	}

	public float getUntranslatedShapeCenterY()
	{
		float scaledY = shapeY;
		scaledY *= scaleFactor.y;
		if (selectedShape == ShapeType.TYPE_RECTANGLE)
		{
			return shapeY + rectHeight / 2;
		}
		else if (selectedShape == ShapeType.TYPE_ELLIPSE || getWorldVertices() == null || getWorldVertices().size() == 0)
		{
			return scaledY;
		}
		else
		{
			return getCentroid(untranslatedVertices).y;
		}
	}

	public PointF getCentroid(ArrayList<PointF> vertices)
	{
		float sumX = 0.0f;
		float sumY = 0.0f;
		for (int i = 0; i < vertices.size(); i++)
		{
			sumX += vertices.get(i).x;
			sumY += vertices.get(i).y;
		}
		float cx = sumX / vertices.size();
		float cy = sumY / vertices.size();
		PointF centroid = new PointF(cx, cy);
		return centroid;
	}

	@Override
	public String toString()
	{
		String selectedShape = "";
		switch (this.selectedShape)
		{
		case TYPE_CHAIN_SHAPE:
			selectedShape = "Chain Shape";
			break;
		case TYPE_ELLIPSE:
			selectedShape = "Ellipse Shape";
			break;
		case TYPE_POLYGON:
			selectedShape = "Polygon Shape";
			break;
		case TYPE_PRISMATIC_AXIS:
			selectedShape = "Prismatic Axis Shape";
			break;
		case TYPE_RECTANGLE:
			selectedShape = "Rectangle Shape";
			break;
		}
		String generalInfo = selectedShape + "  World Start Coords: " + shapeX + ", " + shapeY + "\n";
		String pointInfo = "";
		for (int i = 0; i < worldVertices.size(); i++)
		{
			PointF localCoords = worldVertices.get(i);
			pointInfo += localCoords.x + ", " + localCoords.y + "  ";
		}
		return generalInfo + pointInfo;
	}

	public float getRectWidth()
	{
		return rectWidth;
	}

	public void setRectWidth(float rectWidth)
	{
		this.rectWidth = rectWidth;
	}

	public float getRectHeight()
	{
		return rectHeight;
	}

	public int[] getBezierPoints()
	{
		return bezierPoints;
	}

	public void setBezierPoints(int[] bezierPoints)
	{
		this.bezierPoints = bezierPoints;
	}

	public void setBezierPoints(String points) // Points split by commas
	{
		points = points.replaceAll(" ", "");
		String[] stringIndices = points.split(",");
		int[] bezierIndices = new int[stringIndices.length];
		for (int i = 0; i < bezierIndices.length; i++)
		{
			bezierIndices[i] = Integer.parseInt(stringIndices[i]);
		}
		this.bezierPoints = bezierIndices;
	}

	public void setRectHeight(float rectHeight)
	{
		this.rectHeight = rectHeight;
	}

	public void setShapeX(float shapeX)
	{
		this.shapeX = shapeX;
	}

	public void setShapeY(float shapeY)
	{
		this.shapeY = shapeY;
	}

	public ArrayList<PointF> getUntranslatedVertices()
	{
		return untranslatedVertices;
	}

	public void setUntranslatedVertices(ArrayList<PointF> untranslatedVertices)
	{
		this.untranslatedVertices = untranslatedVertices;
	}

	public Texture getSpriteTexture()
	{
		return spriteTexture;
	}

	public void setSpriteTexture(Texture spriteTexture)
	{
		this.spriteTexture = spriteTexture;
	}

	public BodyDescriptor getBodyDescriptor()
	{
		return bodyDescriptor;
	}

	public void setBodyDescriptor(BodyDescriptor bodyDescriptor)
	{
		this.bodyDescriptor = bodyDescriptor;
	}

	public PointF getScaleFactor()
	{
		return scaleFactor;
	}

	public void setScaleFactor(PointF scaleFactor)
	{
		this.scaleFactor = scaleFactor;
	}

	public PointF getEditorScaleFactor()
	{
		return editorScaleFactor;
	}

	public void setEditorScaleFactor(PointF editorScaleFactor)
	{
		this.editorScaleFactor = editorScaleFactor;
	}

	public Rectangle getShapeBoundingRectangle()
	{
		return shapeBoundingRectangle;
	}

	public void setShapeBoundingRectangle(Rectangle shapeBoundingRectangle)
	{
		this.shapeBoundingRectangle = shapeBoundingRectangle;
	}
}
