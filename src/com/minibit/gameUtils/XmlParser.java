package com.minibit.gameUtils;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class XmlParser
{
	private InputStream fileInputStream;
	private Document xmlDocument;

	public XmlParser(InputStream fileInputStream)
	{
		DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
		try
		{
			DocumentBuilder documentBuilder = builder.newDocumentBuilder();
			xmlDocument = documentBuilder.parse(fileInputStream);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public NodeList loadElements(String searchString)
	{
		return xmlDocument.getElementsByTagName(searchString);
	}
}
