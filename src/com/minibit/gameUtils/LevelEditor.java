package com.minibit.gameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.minibit.gameUtils.LevelEditorShape.ShapeType;
import com.minibit.sprites.PointF;

public class LevelEditor
{
	/*
	 * Define game width and height statically. All vertex manipulations (scaling, translating)
	 * should be done in relation to these values. Libgdx handles all other aspects of screen sizing
	 * for us, thankfully.
	 */
	private final static float DEFAULT_WIDTH = 800, DEFAULT_HEIGHT = 400;
	public final static PointF GAME_SCREEN_DIMENSIONS = new PointF(DEFAULT_WIDTH, DEFAULT_HEIGHT);

	private ArrayList<LevelEditorShape> collisionBoundingShapes = new ArrayList<LevelEditorShape>();
	private PointF editorDimensions;
	private ArrayList<LevelEditorJoint> jointList = new ArrayList<LevelEditorJoint>();
	private NodeList rootNodeList;

	// When an element does not follow the aspect ratio of 2 : 1, we need a scale factor to line
	// vertices up correctly
	private PointF scaleFactor;
	private String imagesPath;
	// The starting location of the sprite on the map
	private PointF spriteStartLoc;
	// The rectangle that defines the area for which a game should end if the sprite is in
	private Rectangle spriteEndArea;
	private String spriteBodyIdentification;
	private PointF mainSpriteBodyDims = new PointF();
	private boolean isMapEditor = false;

	public LevelEditor(String levelResourcePath, String imagesResourcePath, PointF scaleFactor)
	{
		InputStream stream = Gdx.files.internal(levelResourcePath).read();
		XmlParser parser = new XmlParser(stream);
		rootNodeList = parser.loadElements("object");

		editorDimensions = getLevelDimensions(parser);
		this.scaleFactor = scaleFactor;
		this.imagesPath = imagesResourcePath;
		// Cannot contain commas
		this.spriteBodyIdentification = levelResourcePath;

		NodeList mapEditorCheckList = parser.loadElements("map");
		Element element = (Element) mapEditorCheckList.item(0);
		if (element != null)
		{
			String value = getPropertyValue(element, "isMapEditor");
			if (value != null && value.equals("true"))
			{
				isMapEditor = true;
			}
		}
	}

	private static final String POLYGON_ID = "polygon";
	private static final String ELLIPSE_ID = "wheel";
	private static final String RECTANGLE_ID = "rect";
	private static final String CHAIN_SHAPE_ID = "polyline";
	private static final String BOUNDS_KEY = "Start Rect";
	private static final String JOINT_KEY = "Joint";
	private static final String TEXTURE_ID = "texture";
	private static final String MAP_SPRITE_LOC_ID = "mapSpriteLoc";

	public void generateShapes()
	{
		float trueStartX = 0;
		float trueStartY = 0;

		ArrayList<Tuple<Element, String>> boundingRectList = new ArrayList<Tuple<Element, String>>();
		for (int i = 0; i < rootNodeList.getLength(); i++)
		{
			Element element = (Element) rootNodeList.item(i);
			String name = element.getAttribute("name");
			if (name != null && name.equals(BOUNDS_KEY))
			{
				Element parentNode = (Element) element.getParentNode();
				String layerId = parentNode.getAttribute("name");
				if (layerId.equals("Object Layer 1"))
				{
					trueStartX = Float.parseFloat(element.getAttribute("x"));
					trueStartY = Float.parseFloat(element.getAttribute("y"));
					float width = Float.parseFloat(element.getAttribute("width"));
					float height = Float.parseFloat(element.getAttribute("height"));
					editorDimensions.x = width;
					editorDimensions.y = height;
				}
				else
				{
					boundingRectList.add(new Tuple<Element, String>(element, layerId));
				}
			}
		}

		mainSpriteBodyDims = new PointF(editorDimensions.x, editorDimensions.y);

		for (int i = 0; i < rootNodeList.getLength(); i++)
		{
			Element element = (Element) rootNodeList.item(i);

			int worldXCoords = Integer.parseInt(element.getAttribute("x"));
			int worldYCoords = Integer.parseInt(element.getAttribute("y"));
			worldXCoords -= trueStartX;
			worldYCoords -= trueStartY;

			PointF worldCoordLoc = new PointF(worldXCoords, worldYCoords);
			scalePoint(worldCoordLoc, GAME_SCREEN_DIMENSIONS, editorDimensions);

			String bodyId = getPropertyValue(element, "bodyId");
			String textureId = getPropertyValue(element, TEXTURE_ID);
			String bodyType = getPropertyValue(element, "bodyType");

			// For chain shapes / maps, when we want a smooth bezier curve between every 3 points
			// specified. 2nd point specified is the control point.
			String bezierMapPoints = getPropertyValue(element, "bezier");

			boolean isDestructionBody = Boolean.parseBoolean(getPropertyValue(element,
					"subDestructionBody"));

			Element parentNode = (Element) element.getParentNode();
			String layerId = parentNode.getAttribute("name");
			String groupTextureId = null;

			Texture shapeTexture = getTexture(imagesPath, textureId);
			Texture groupTexture = getTexture(imagesPath, groupTextureId);

			if (element.getAttribute("type").equals(MAP_SPRITE_LOC_ID))
			{
				float x = worldCoordLoc.x;
				float y = worldCoordLoc.y;
				if (element.getAttribute("name").equals("startLoc"))
				{
					spriteStartLoc = new PointF(x, y);
				}
				else if (element.getAttribute("name").equals("endLoc"))
				{
					int width = Integer.parseInt(element.getAttribute("width"));
					int height = Integer.parseInt(element.getAttribute("height"));
					PointF worldDims = new PointF(width, height);
					scalePoint(worldDims, GAME_SCREEN_DIMENSIONS, editorDimensions);
					spriteEndArea = new Rectangle(x, y - height, x + width, y);
				}
			}

			BodyDescriptor bodyDescriptor = new BodyDescriptor(layerId, bodyId,
					spriteBodyIdentification);
			bodyDescriptor.setBodyType(bodyType);
			bodyDescriptor.setMapBody(isMapEditor);
			bodyDescriptor.setExtraProperties(getCompletePropertyList(element));
			bodyDescriptor.setSubDestructionBody(isDestructionBody);

			if (element.getAttribute("type").equals(ELLIPSE_ID))
			{
				bodyDescriptor.setShapeType(LevelEditorShape.ShapeType.TYPE_ELLIPSE);
				int width = Integer.parseInt(element.getAttribute("width"));
				PointF dims = new PointF(width, width);
				scalePoint(dims, GAME_SCREEN_DIMENSIONS, editorDimensions);
				LevelEditorShape shape = new LevelEditorShape(worldCoordLoc, dims, scaleFactor,
						bodyDescriptor);

				if (shapeTexture != null)
				{
					shape.setSpriteTexture(shapeTexture);
				}

				collisionBoundingShapes.add(shape);
			}
			else if (element.getAttribute("type").equals(RECTANGLE_ID))
			{
				bodyDescriptor.setShapeType(ShapeType.TYPE_RECTANGLE);
				float width = Integer.parseInt(element.getAttribute("width")) / editorDimensions.x
						* GAME_SCREEN_DIMENSIONS.x;
				float height = Integer.parseInt(element.getAttribute("height"))
						/ editorDimensions.y * GAME_SCREEN_DIMENSIONS.y;

				PointF dims = new PointF(width, height);

				worldCoordLoc.y = GAME_SCREEN_DIMENSIONS.y - worldCoordLoc.y;
				LevelEditorShape shape = new LevelEditorShape(worldCoordLoc, dims, scaleFactor,
						bodyDescriptor);
				shape.setSpriteTexture(shapeTexture);

				shape.createAndSetRectangleVertices(dims);
				collisionBoundingShapes.add(shape);
			}
			else if (element.hasChildNodes())
			{
				NodeList secondaryNodes = element.getChildNodes();
				for (int j = 0; j < secondaryNodes.getLength(); j++)
				{
					if (secondaryNodes.item(j).hasAttributes())
					{
						Element shapeElement = (Element) secondaryNodes.item(j);
						Element possibleShapeBounds = isShapeBounded(boundingRectList, layerId);
						float boundsX = 0, boundsY = 0, boundsWidth = 0, boundsHeight = 0;

						if (possibleShapeBounds != null)
						{
							boundsX = Float.parseFloat(possibleShapeBounds.getAttribute("x"));
							boundsY = Float.parseFloat(possibleShapeBounds.getAttribute("y"));
							boundsWidth = Float.parseFloat(possibleShapeBounds
									.getAttribute("width"));
							boundsHeight = Float.parseFloat(possibleShapeBounds
									.getAttribute("height"));
						}

						PointF newDims = new PointF(editorDimensions.x, editorDimensions.y);
						if (possibleShapeBounds != null)
						{
							worldXCoords = (int) (worldXCoords - boundsX);
							float localY = worldYCoords - boundsY;
							worldYCoords = (int) (localY + editorDimensions.y - boundsHeight);
							newDims.x = boundsWidth;
							newDims.y = boundsHeight;
						}

						LevelEditorShape shape = null;
						if (shapeElement.getTagName().equals(POLYGON_ID))
						{
							bodyDescriptor.setShapeType(ShapeType.TYPE_POLYGON);
						}
						else if (shapeElement.getTagName().equals(CHAIN_SHAPE_ID))
						{
							bodyDescriptor
									.setShapeType(LevelEditorShape.ShapeType.TYPE_CHAIN_SHAPE);
						}

						shape = new LevelEditorShape(worldXCoords, worldYCoords, bodyDescriptor);
						if (possibleShapeBounds != null)
						{
							PointF loc = new PointF(boundsX, boundsY + boundsHeight);
							LevelEditor.scalePoint(loc, GAME_SCREEN_DIMENSIONS, editorDimensions);
							LevelEditor.scalePoint(newDims, GAME_SCREEN_DIMENSIONS,
									editorDimensions);
							newDims.x *= scaleFactor.x;
							newDims.y *= scaleFactor.y;
							loc.x = loc.x * scaleFactor.x;
							loc.y = loc.y * scaleFactor.y;
							Rectangle boundRectangle = new Rectangle(loc.x, loc.y, newDims.x,
									newDims.y / 2);
							shape.setShapeBoundingRectangle(boundRectangle);
						}

						if (shapeTexture != null)
						{
							shape.setSpriteTexture(shapeTexture);
						}
						else if (groupTexture != null)
						{
							shape.setSpriteTexture(groupTexture);
						}

						if (bezierMapPoints != null)
						{
							shape.setBezierPoints(bezierMapPoints);
						}

						ArrayList<PointF> points = pointsForElement(shapeElement);
						// These coords should not be scaled!
						shape.setVertices(points, scaleFactor, GAME_SCREEN_DIMENSIONS,
								editorDimensions);
						collisionBoundingShapes.add(shape);
					}
				}
			}
		}

		// Joints must be found only after all other bodies have been added to
		// "collisionBoundingShapes"
		for (int i = 0; i < rootNodeList.getLength(); i++)
		{
			Element element = (Element) rootNodeList.item(i);
			String possibleJointKey = element.getAttribute("name");

			Element parentNode = (Element) rootNodeList.item(i).getParentNode();
			String layerId = parentNode.getAttribute("name");

			if (possibleJointKey.equals(JOINT_KEY))
			{
				int worldXCoords = Integer.parseInt(element.getAttribute("x"));
				int worldYCoords = Integer.parseInt(element.getAttribute("y"));
				worldXCoords -= trueStartX;
				worldYCoords -= trueStartY;

				int width = Integer.parseInt(element.getAttribute("width"));
				PointF dims = new PointF(width, width);
				scalePoint(dims, GAME_SCREEN_DIMENSIONS, editorDimensions);
				PointF worldCoordLoc = new PointF(worldXCoords, worldYCoords);
				scalePoint(worldCoordLoc, GAME_SCREEN_DIMENSIONS, editorDimensions);

				String jointType = element.getAttribute("type");
				String jointId = getPropertyValue(element, "jointId");
				LevelEditorShape prismaticAxisShape = null;

				BodyDescriptor descriptor = new BodyDescriptor(layerId, jointId, null);
				descriptor.setExtraProperties(getCompletePropertyList(element));
				descriptor.setShapeType(LevelEditorShape.ShapeType.TYPE_JOINT);
				descriptor.setMapBody(isMapEditor);

				if (jointType.equals(LevelEditorJoint.TYPE_PRISMATIC_JOINT))
				{
					String prismaticAxis = getPrismaticAxis(element);
					prismaticAxisShape = getPrismaticAxisShape(prismaticAxis);
				}
				String[] connectionsList = getAssociatedConnections(element);
				LevelEditorJoint jointShape = new LevelEditorJoint(worldCoordLoc, dims,
						scaleFactor, jointType, getAssociatedBodies(connectionsList), descriptor);
				if (prismaticAxisShape != null)
				{
					jointShape.setPrismaticAxis(prismaticAxisShape);
				}
				jointList.add(jointShape);
			}
		}
	}

	private Element isShapeBounded(ArrayList<Tuple<Element, String>> boundingRects,
			String shapeLayerId)
	{
		for (int i = 0; i < boundingRects.size(); i++)
		{
			String rectLayerId = boundingRects.get(i).getValue();
			if (rectLayerId.equals(shapeLayerId))
			{
				return boundingRects.get(i).getKey();
			}
		}
		return null;
	}

	private ArrayList<PointF> pointsForElement(Element shapeElement)
	{
		String attributesList = shapeElement.getAttribute("points");
		String[] attrs = attributesList.split(" ");
		ArrayList<PointF> listPoints = new ArrayList<PointF>();
		for (int i = 0; i < attrs.length; i++)
		{
			int x = Integer.parseInt(attrs[i].split(",")[0]);
			int y = Integer.parseInt(attrs[i].split(",")[1]);
			listPoints.add(new PointF(x, y));
		}
		return listPoints;
	}

	private float mapMinY;

	public void createMap(World world)
	{
		generateShapes();
		if (collisionBoundingShapes.size() > 0)
		{
			ArrayList<Float> subMinY = new ArrayList<Float>();
			for (LevelEditorShape shape : collisionBoundingShapes)
			{
				ArrayList<PointF> vertices = shape.getWorldVertices();
				if (vertices != null && vertices.size() > 1)
				{
					subMinY.add(getMinY(vertices));
				}
				BodyDef staticBodyDef = new BodyDef();
				staticBodyDef.type = BodyType.StaticBody;
				Body body = null;
				if (shape.getType() == LevelEditorShape.ShapeType.TYPE_CHAIN_SHAPE)
				{
					body = world.createBody(staticBodyDef);
					ChainShape chainShape = new ChainShape();
					chainShape.createChain(convertListToArray(vertices));
					FixtureDef def = new FixtureDef();
					def.shape = chainShape;
					body.createFixture(def);
					chainShape.dispose();
				}
			}

			Collections.sort(subMinY);
			mapMinY = subMinY.get(0);
		}
	}

	public static float getMinY(ArrayList<PointF> vertices)
	{
		float minY = vertices.get(0).y;
		for (PointF pointF : vertices)
		{
			if (pointF.y < minY)
			{
				minY = pointF.y;
			}
		}
		return minY;
	}

	public static String getPropertyValue(Element objectElement, String propertyName)
	{
		if (objectElement.hasChildNodes())
		{
			NodeList propertyList = objectElement.getElementsByTagName("property");
			for (int i = 0; i < propertyList.getLength(); i++)
			{
				Element childProperty = (Element) propertyList.item(i);
				String pName = childProperty.getAttribute("name");
				if (pName != null && pName.equals(propertyName))
				{
					return childProperty.getAttribute("value");
				}
			}
		}
		return null;
	}

	public static ArrayList<Tuple<String, String>> getCompletePropertyList(Element objectElement)
	{
		ArrayList<Tuple<String, String>> completePList = new ArrayList<Tuple<String, String>>();
		if (objectElement.hasChildNodes())
		{
			NodeList propertyList = objectElement.getElementsByTagName("property");
			for (int i = 0; i < propertyList.getLength(); i++)
			{
				Element childProperty = (Element) propertyList.item(i);
				String pName = childProperty.getAttribute("name");
				String attribute = childProperty.getAttribute("value");
				completePList.add(new Tuple<String, String>(pName, attribute));
			}
		}
		return completePList;
	}

	public static ChainShape getChainShape(ArrayList<PointF> vertices)
	{
		ChainShape chainShape = new ChainShape();
		chainShape.createChain(convertListToArray(vertices));
		return chainShape;
	}

	public static PolygonShape getPolygonShape(ArrayList<PointF> vertices)
	{
		PolygonShape polyShape = new PolygonShape();
		if (!areVerticesClockwiseOrientation(vertices))
		{
			Collections.reverse(vertices);
		}
		Vector2[] points = convertListToArray(vertices);
		polyShape.set(points);
		return polyShape;
	}

	// Winding has to be counter-clockwise.
	public static boolean areVerticesClockwiseOrientation(ArrayList<PointF> vertices)
	{
		float totalArea = 0.0f;
		int j = 0;
		for (int i = 0; i < vertices.size(); i++)
		{
			j = (i + 1) % vertices.size();
			totalArea += vertices.get(j).x * vertices.get(i).y - vertices.get(i).x
					* vertices.get(j).y;
		}

		float correctArea = totalArea / 2;

		if (correctArea > 0)
		{
			return false;
		}

		return true;
	}

	public static void scalePoint(PointF pointToScale, PointF gameScreenDims, PointF editorDims)
	{
		float unscaledX = pointToScale.x;
		float unscaledY = pointToScale.y;
		float scaledX = unscaledX / editorDims.x * gameScreenDims.x;
		float scaledY = unscaledY / editorDims.y * gameScreenDims.y;
		scaledY = gameScreenDims.y - scaledY;
		pointToScale.set(scaledX, scaledY);
	}

	private PointF getLevelDimensions(XmlParser parser)
	{
		NodeList dimensionsList = parser.loadElements("map");
		Element dimensionTag = (Element) dimensionsList.item(0);
		float width = Float.parseFloat(dimensionTag.getAttribute("width"));
		float height = Float.parseFloat(dimensionTag.getAttribute("height"));
		float scaleX = Float.parseFloat(dimensionTag.getAttribute("tilewidth"));
		float scaleY = Float.parseFloat(dimensionTag.getAttribute("tileheight"));
		return new PointF(width * scaleX, height * scaleY);
	}

	private LevelEditorShape[] getAssociatedBodies(String[] bodyIds)
	{
		ArrayList<LevelEditorShape> collisionBodies = getCollisionBoundingShapes();
		LevelEditorShape[] associatedBodies = new LevelEditorShape[2];
		int bodyIndex = 0;
		for (int j = 0; j < collisionBodies.size(); j++)
		{
			String bodyId = collisionBodies.get(j).getBodyDescriptor().getBodyId();
			if (bodyId != null && !bodyId.equals(""))
			{
				for (int i = 0; i < bodyIds.length; i++)
				{
					if (bodyId.equals(bodyIds[i]))
					{
						associatedBodies[bodyIndex] = collisionBodies.get(j);
						bodyIndex++;
					}
				}
			}
			if (bodyIndex == 2)
			{
				break;
			}
		}
		return associatedBodies;
	}

	private String[] getAssociatedConnections(Element root)
	{
		String connections = LevelEditor.getPropertyValue(root, "connectedTo");
		String[] connectionsArray = connections.split(",");
		for (int i = 0; i < connectionsArray.length; i++)
		{
			// Get rid of spaces at start of name
			String currentConnection = connectionsArray[i];
			int startSpaceCount = 0;
			for (int j = 0; j < currentConnection.length(); j++)
			{
				if (currentConnection.substring(j, j + 1).equals(" "))
				{
					startSpaceCount++;
				}
				else
				{
					break;
				}
			}
			if (startSpaceCount < currentConnection.length())
			{
				currentConnection = currentConnection.substring(startSpaceCount);
				connectionsArray[i] = currentConnection;
			}
		}
		return connectionsArray;
	}

	public String getPrismaticAxis(Element root)
	{
		NodeList childNodes = root.getElementsByTagName("property");
		for (int i = 0; i < childNodes.getLength(); i++)
		{
			Element child = (Element) childNodes.item(i);
			if (child.getAttribute("name").equals("prismaticAxis"))
			{
				String prismaticAxis = child.getAttribute("value");
				return prismaticAxis;
			}
		}
		return null;
	}

	public LevelEditorShape getPrismaticAxisShape(String prismaticAxisId)
	{
		for (int i = 0; i < collisionBoundingShapes.size(); i++)
		{
			String bodyId = collisionBoundingShapes.get(i).getBodyDescriptor().getBodyId();
			if (bodyId != null && bodyId.equals(prismaticAxisId))
			{
				collisionBoundingShapes.get(i).setType(
						LevelEditorShape.ShapeType.TYPE_PRISMATIC_AXIS);
				LevelEditorShape prismAxisShape = collisionBoundingShapes.get(i);
				LevelEditorShape axisCopy = new LevelEditorShape(prismAxisShape);
				collisionBoundingShapes.remove(i);
				return axisCopy;
			}
		}
		return null;
	}

	public static Vector2[] convertListToArray(ArrayList<PointF> vertices)
	{
		Vector2[] arrayPoints = new Vector2[vertices.size()];
		for (int i = 0; i < vertices.size(); i++)
		{
			arrayPoints[i] = new Vector2(vertices.get(i).x, vertices.get(i).y);
		}
		return arrayPoints;
	}

	private static String readFileContents(String resourcePath)
	{
		InputStream stream = Gdx.files.internal(resourcePath).read();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String s = "";
		String fileContents = "";
		try
		{
			while ((s = reader.readLine()) != null)
			{
				fileContents += s;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return fileContents;
	}

	public Texture getTexture(String textureSubFolder, String textureId)
	{
		if (textureId != null && !textureId.equals(""))
		{
			if (imagesPath != null && !imagesPath.equals(""))
			{
				return new Texture(Gdx.files.internal(textureSubFolder + "/" + textureId));
			}
		}
		return null;
	}

	public static PointF calcScaleFactor(Sprite sprite, PointF gameScreenDims)
	{
		float xScale = 1.0f / (gameScreenDims.x / sprite.getWidth());
		float yScale = 1.0f / (gameScreenDims.y / sprite.getHeight());
		return new PointF(xScale, yScale);
	}

	public ArrayList<LevelEditorShape> getCollisionBoundingShapes()
	{
		return collisionBoundingShapes;
	}

	public PointF getMainSpriteDims()
	{
		return mainSpriteBodyDims;
	}

	public void setMainSpriteDims(PointF spriteBoundsDimensions)
	{
		this.mainSpriteBodyDims = spriteBoundsDimensions;
	}

	public void setCollisionBoundingShapes(ArrayList<LevelEditorShape> collisionBoundingShapes)
	{
		this.collisionBoundingShapes = collisionBoundingShapes;
	}

	public ArrayList<LevelEditorJoint> getJointList()
	{
		return jointList;
	}

	public void setJointList(ArrayList<LevelEditorJoint> jointList)
	{
		this.jointList = jointList;
	}

	public PointF getSpriteStartLoc()
	{
		return spriteStartLoc;
	}

	public void setSpriteStartLoc(PointF spriteStartLoc)
	{
		this.spriteStartLoc = spriteStartLoc;
	}

	public Rectangle getSpriteEndLoc()
	{
		return spriteEndArea;
	}

	public void setSpriteEndLoc(Rectangle spriteEndLoc)
	{
		this.spriteEndArea = spriteEndLoc;
	}

	public float getMapMinY()
	{
		return mapMinY;
	}

	public void setMapMinY(float mapMinY)
	{
		this.mapMinY = mapMinY;
	}
}
