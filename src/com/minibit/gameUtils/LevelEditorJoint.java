package com.minibit.gameUtils;

import java.util.ArrayList;

import com.minibit.sprites.PointF;

public class LevelEditorJoint extends LevelEditorShape
{
	private LevelEditorShape[] connectedBodies = new LevelEditorShape[2];
	private String jointType = "";
	public static final String TYPE_REVOLUTE_JOINT = "Revolute Joint";
	public static final String TYPE_PRISMATIC_JOINT = "Prismatic Joint";
	public static final String TYPE_WELD_JOINT = "Weld Joint";
	public static final String TYPE_DISTANCE_JOINT = "Distance Joint";
	private LevelEditorShape prismaticAxis;

	public LevelEditorJoint(int x, int y, String jointType, LevelEditorShape[] connectedBodies, BodyDescriptor jointId)
	{
		super(x, y, jointId);
		this.jointType = jointType;
		this.connectedBodies = connectedBodies;
	}

	public LevelEditorJoint(LevelEditorShape shape, String jointType, LevelEditorShape[] connectedBodies)
	{
		super(shape);
		this.jointType = jointType;
		this.connectedBodies = connectedBodies;
	}

	public LevelEditorJoint(PointF loc, PointF dims, PointF scaleFactor, String jointType, LevelEditorShape[] connectedBodies, BodyDescriptor jointId)
	{
		super(loc, dims, scaleFactor, jointId);
		this.jointType = jointType;
		this.connectedBodies = connectedBodies;
	}

	public LevelEditorJoint(int x, int y, ArrayList<PointF> vertices, String jointType, LevelEditorShape[] connectedBodies, BodyDescriptor jointId)
	{
		super(x, y, vertices, ShapeType.TYPE_JOINT, jointId);
		this.jointType = jointType;
		this.connectedBodies = connectedBodies;
	}

	public LevelEditorShape[] getConnectedBodies()
	{
		return connectedBodies;
	}

	public void setConnectedBodies(LevelEditorShape[] connectedBodies)
	{
		this.connectedBodies = connectedBodies;
	}

	public String getJointType()
	{
		return jointType;
	}

	public void setJointType(String jointType)
	{
		this.jointType = jointType;
	}

	public LevelEditorShape getPrismaticAxis()
	{
		return prismaticAxis;
	}

	public void setPrismaticAxis(LevelEditorShape prismaticAxis)
	{
		this.prismaticAxis = prismaticAxis;
	}

}
