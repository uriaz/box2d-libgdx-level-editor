package com.minibit.sprites;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.minibit.box2dLevelEditor.BasePhysicsGame;
import com.minibit.gameUtils.ActivationTriggerHolder;
import com.minibit.gameUtils.BodyDescriptor;
import com.minibit.gameUtils.LevelEditor;
import com.minibit.gameUtils.LevelEditorJoint;
import com.minibit.gameUtils.LevelEditorShape;
import com.minibit.gameUtils.Tuple;

public class PhysicsSprite extends Sprite
{
	private World physicsWorld;
	private Body mainSpriteBody;
	private LevelEditor spriteEditor;
	private ActivationTriggerHolder mActivationTriggers;

	private ArrayList<PhysicsSprite> subBodies = new ArrayList<PhysicsSprite>();
	private ArrayList<Tuple<Joint, String>> jointList = new ArrayList<Tuple<Joint, String>>();

	public final static float PIXEL_METER_RATIO = 60;

	/*
	 * Sprite types needed because of different sprite.getBody.getPosition returns. Wheel shapes' positions are returned as their centers, while other
	 * bodies' positions are returned as their origins, or lower-left corners.
	 */
	public final static int WHEEL_SPRITE = -1;
	public final static int MAIN_SPRITE = 1;
	public final static int POLYGON_SPRITE = 3;

	private final static float DENSITY = 0.01f;
	private final static float FRICTION = 4.0f;

	private SpriteListener spriteListener;
	private PhysicsSprite activationTriggerer;
	// If the sprite is outside this rectangle, a callback is fired
	private Rectangle spriteFailureBounds;
	private Rectangle spriteBoundingRectangle;
	private Rectangle spriteSuccessArea;

	private int spriteType = 0;

	// Only allow subclasses to access this constructor
	protected PhysicsSprite(Sprite sprite, Body subBody)
	{
		super(sprite);
		this.mainSpriteBody = subBody;
	}

	/**
	 * Convenience method to instantiate a physics sprite from a libgdx sprite straight away.
	 * 
	 * @param sprite
	 *            Non-null sprite that has been set a texture or a width / height.
	 * @param lvEditorPath
	 *            The path to the sprites .tmx file.
	 * @param physicsWorld
	 *            The physics world to place this sprite in.
	 */
	public PhysicsSprite(Sprite sprite, String lvEditorPath, World physicsWorld)
	{
		super(sprite);
		this.physicsWorld = physicsWorld;

		PointF spriteScaleFactor = getMeterScaleFactor(sprite);
		LevelEditor levelEditor = new LevelEditor(lvEditorPath, BasePhysicsGame.IMAGE_RESOURCE_PATH, spriteScaleFactor);
		levelEditor.generateShapes();
		this.spriteEditor = levelEditor;
	}

	/**
	 * Constructor to initialize a physics sprite from its .tmx descriptor and a path to its texture.
	 * 
	 * @param spriteImagePath
	 *            Path to the sprite's texture.
	 * @param spriteLvEditorPath
	 *            Path to the sprite's .tmx descriptor.
	 * @param physicsWorld
	 *            Physics world to place this sprite in.
	 */
	public PhysicsSprite(String spriteImagePath, String spriteLvEditorPath, World physicsWorld)
	{
		super();
		this.physicsWorld = physicsWorld;

		Sprite tempSprite = initSprite(spriteImagePath);
		set(tempSprite);

		// Scale the sprite and its corresponding texture down by a factor of around 60 so Box2D thinks its using meters.
		PointF spriteScaleFactor = getMeterScaleFactor(tempSprite);

		/*
		 * Because we scaled the texture down, we now also have to scale vertex data for the sprite, so we calculate a scale factor and pass it along
		 * to the level editor.
		 */
		LevelEditor levelEditor = new LevelEditor(spriteLvEditorPath, BasePhysicsGame.IMAGE_RESOURCE_PATH, spriteScaleFactor);
		levelEditor.generateShapes();
		this.spriteEditor = levelEditor;
	}

	/**
	 * Scales a sprite down by a factor of around 60 so B2D thinks its using meters. Should be overriden if certain bodies shouldn't be scaled.
	 * 
	 * @param spriteImagePath
	 * @param spriteLvEditorPath
	 * @return
	 */
	public PointF getMeterScaleFactor(Sprite unscaledSprite)
	{
		return LevelEditor.calcScaleFactor(unscaledSprite, LevelEditor.GAME_SCREEN_DIMENSIONS);
	}

	public Sprite initSprite(String texture)
	{
		Texture loadedTexture = new Texture(Gdx.files.internal(texture));
		Sprite tempSprite = new Sprite(loadedTexture);
		tempSprite.setSize(tempSprite.getWidth() / PhysicsSprite.PIXEL_METER_RATIO, tempSprite.getHeight() / PhysicsSprite.PIXEL_METER_RATIO);
		return tempSprite;
	}

	// This method should be overriden in each subclass.
	public void initSprite()
	{
	}

	/*
	 * Needed because of slight delay at first iteration; messes with bounds checking if not there.
	 */
	private boolean firstUpdate = true;

	private Vector2 mainSpriteBodyPreviousPosition = new Vector2();
	private float mainSpriteBodyPreviousAngle = 0.0f;

	public void smoothStateUpdate(float fixedTimeStepRatio)
	{
		if (mainSpriteBody != null)
		{
			float oneMinusRatio = 1.0f - fixedTimeStepRatio;
			float x = fixedTimeStepRatio * mainSpriteBody.getPosition().x + oneMinusRatio * mainSpriteBodyPreviousPosition.x;
			float y = fixedTimeStepRatio * mainSpriteBody.getPosition().y + oneMinusRatio * mainSpriteBodyPreviousPosition.y;
			float rotation = (float) (mainSpriteBody.getAngle() * fixedTimeStepRatio + oneMinusRatio * mainSpriteBodyPreviousAngle);

			if (spriteFailureBounds != null && !spriteFailureBounds.contains(getX(), getY()) && !firstUpdate)
			{
				spriteListener.spriteOutOfBounds(this);
				return;
			}
			else if (spriteSuccessArea != null && spriteSuccessArea.contains(getX(), getY()) && !firstUpdate)
			{
				spriteListener.spriteReachedSuccessArea(this);
			}

			if (spriteType == WHEEL_SPRITE || spriteType == POLYGON_SPRITE)
			{
				setX(x);
				setY(y);
			}
			else
			{
				super.setX(x);
				super.setY(y);
			}
			setRotation((float) (rotation * 180.0f / Math.PI));
			// System.out.println(((BodyDescriptor) mainSpriteBody.getUserData()).getBodyId() + ", "
			// + getX() + ", " + getY());
		}

		for (PhysicsSprite pSprite : subBodies)
		{
			pSprite.smoothStateUpdate(fixedTimeStepRatio);
		}

		checkActivationTriggerState();

		firstUpdate = false;
	}

	public void resetSmoothStates()
	{
		if (mainSpriteBody != null)
		{
			mainSpriteBodyPreviousPosition.x = mainSpriteBody.getPosition().x;
			mainSpriteBodyPreviousPosition.y = mainSpriteBody.getPosition().y;
			mainSpriteBodyPreviousAngle = mainSpriteBody.getAngle();

			float rotation = mainSpriteBodyPreviousAngle;
			float x = mainSpriteBodyPreviousPosition.x;
			float y = mainSpriteBodyPreviousPosition.y;

			if (spriteType == WHEEL_SPRITE || spriteType == POLYGON_SPRITE)
			{
				setX(x);
				setY(y);
			}
			else
			{
				super.setX(x);
				super.setY(y);
			}

			setRotation((float) (rotation * 180.0f / Math.PI));
		}

		for (int i = 0; i < subBodies.size(); i++)
		{
			subBodies.get(i).resetSmoothStates();
		}
	}

	public void handleUserInput()
	{

	}

	// Sprite's x and y locs won't be available until after a few update cycles, so we must wait
	// until they are before initing activation triggers
	private boolean canCreateActivationTriggers = false;

	public void checkActivationTriggerState()
	{
		// Only create triggers after locations have been set
		if (activationTriggerer != null && !canCreateActivationTriggers && activationTriggerer.getX() != 0 && activationTriggerer.getY() != 0)
		{
			canCreateActivationTriggers = true;
			createActivationTriggerForSprite(spriteListener, activationTriggerer);
		}
		else if (canCreateActivationTriggers)
		{
			if (spriteListener != null && activationTriggerer != null && mActivationTriggers != null)
			{
				Iterator<String> keys = mActivationTriggers.getKeys();
				while (keys.hasNext())
				{
					String key = (String) keys.next();
					float actValue = mActivationTriggers.getActivationValue(key);
					if (actValue != ActivationTriggerHolder.NO_TRIGGER)
					{
						Method getter = mActivationTriggers.getMethodForKey(key);
						try
						{
							float realTimeValue = ((Float) getter.invoke(activationTriggerer)).floatValue();
							if (realTimeValue > actValue)
							{
								mActivationTriggers.setActivationValue(key, ActivationTriggerHolder.NO_TRIGGER);
								spriteListener.activationTriggered(this);
							}
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	@Override
	public void draw(SpriteBatch batch)
	{
		if (getTexture() != null)
		{
			super.draw(batch);
		}

		for (PhysicsSprite pSprite : subBodies)
		{
			pSprite.draw(batch);
		}
	}

	public void drawSpriteBoundingBox(ShapeRenderer renderer)
	{
		renderer.begin(ShapeType.Line);
		Rectangle spriteRect = spriteBoundingRectangle;
		if (spriteRect != null)
		{
			renderer.rect(spriteRect.x, spriteRect.y, spriteRect.width, spriteRect.height);
		}
		renderer.end();
	}

	// Needed for bodies that shouldn't be translated when built.
	private Body initBody(World world, LevelEditorShape shape)
	{
		BodyDef bodyDef = createBodyDef(shape);
		Body body = world.createBody(bodyDef);
		body.setUserData(shape.getBodyDescriptor());
		FixtureDef def = createFixtureDef(shape);
		body.createFixture(def);
		def.shape.dispose();
		return body;
	}

	public Body initPhysicsBody(World world, LevelEditorShape shape)
	{
		if (shape.getType() != LevelEditorShape.ShapeType.TYPE_PRISMATIC_AXIS)
		{
			BodyDef bodyDef = createBodyDef(shape);
			bodyDef.position.x = shape.getUntranslatedShapeCenterX();
			bodyDef.position.y = shape.getUntranslatedShapeCenterY();

			Body body = world.createBody(bodyDef);
			body.setUserData(shape.getBodyDescriptor());

			FixtureDef def = createFixtureDef(shape);
			body.createFixture(def);
			def.shape.dispose();

			return body;
		}

		return null;
	}

	// If body is part of main body as a fixture
	private final static String CONNECTED_BODY_ID = "C*";
	private final static String BASE_LAYER_ID = "Object Layer 1";

	public void createSpriteBody()
	{
		ArrayList<LevelEditorShape> shapes = spriteEditor.getCollisionBoundingShapes();
		for (LevelEditorShape shape : shapes)
		{
			if (shape == null)
			{
				continue;
			}

			String bodyId = shape.getBodyDescriptor().getBodyId();
			if (shape.getType() == LevelEditorShape.ShapeType.TYPE_ELLIPSE && !bodyId.contains(CONNECTED_BODY_ID))
			{
				Body subBody = initPhysicsBody(physicsWorld, shape);
				if (shape.getSpriteTexture() != null)
				{
					Sprite texSprite = new Sprite(shape.getSpriteTexture());
					texSprite.setSize(shape.getRectWidth(), shape.getRectWidth());
					texSprite.setOrigin((texSprite.getX() + texSprite.getWidth() / 2), (texSprite.getHeight() / 2));
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = WHEEL_SPRITE;
					subBodies.add(subSprite);
				}
				else
				{
					Sprite texSprite = new Sprite();
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = WHEEL_SPRITE;
					subBodies.add(subSprite);
				}
			}
			else if (shape.getType() == LevelEditorShape.ShapeType.TYPE_RECTANGLE && !bodyId.contains(CONNECTED_BODY_ID))
			{
				Body subBody = initPhysicsBody(physicsWorld, shape);
				if (shape.getSpriteTexture() != null)
				{
					Sprite texSprite = new Sprite(shape.getSpriteTexture());
					texSprite.setSize(shape.getRectWidth(), shape.getRectHeight());
					texSprite.setOrigin((texSprite.getX() + texSprite.getWidth() / 2), (texSprite.getHeight() / 2));
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = POLYGON_SPRITE;
					subBodies.add(subSprite);
				}
				else
				{
					Sprite texSprite = new Sprite();
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = POLYGON_SPRITE;
					subBodies.add(subSprite);
				}
			}
			// Initialize bodies that aren't connected to fixtures
			else if (bodyId != null && !bodyId.contains(CONNECTED_BODY_ID) && shape.getType() == LevelEditorShape.ShapeType.TYPE_POLYGON)
			{
				shape.translatePolygon(new PointF(0.0f, 0.0f));

				Body subBody = initPhysicsBody(physicsWorld, shape);
				// Always specify a texture or else body will be oriented incorrectly!
				if (shape.getSpriteTexture() != null)
				{
					Sprite texSprite = new Sprite(shape.getSpriteTexture());
					texSprite.setOrigin(texSprite.getWidth() / PIXEL_METER_RATIO / 2, texSprite.getHeight() / PIXEL_METER_RATIO / 2);
					texSprite.setSize(texSprite.getWidth() / PIXEL_METER_RATIO, texSprite.getHeight() / PIXEL_METER_RATIO);
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = POLYGON_SPRITE;
					subBodies.add(subSprite);
				}
				else
				{
					Sprite texSprite = new Sprite();
					texSprite.setSize(texSprite.getWidth() / PIXEL_METER_RATIO / 2, texSprite.getHeight() / PIXEL_METER_RATIO / 2);
					PhysicsSprite subSprite = new PhysicsSprite(texSprite, subBody);
					subSprite.spriteType = POLYGON_SPRITE;
					subBodies.add(subSprite);
				}
			}
			// Initialize bodies that are attached to fixtures
			else if (shape.getType() == LevelEditorShape.ShapeType.TYPE_POLYGON)
			{
				String layerId = shape.getBodyDescriptor().getLayerId();
				if (layerId.equals(BASE_LAYER_ID))
				{
					if (mainSpriteBody == null)
					{
						// In case the first fixture of the body has a bodyId and is the fixture for
						// main body.
						shape.getBodyDescriptor().getFixtureIds().add(shape.getBodyDescriptor().getBodyId());
						mainSpriteBody = initBody(physicsWorld, shape);
					}
					else
					{
						BodyDescriptor ud = (BodyDescriptor) mainSpriteBody.getUserData();
						ud.getFixtureIds().add(shape.getBodyDescriptor().getBodyId());
						mainSpriteBody.setUserData(ud);

						PolygonShape polyShape = LevelEditor.getPolygonShape(shape.getWorldVertices());
						FixtureDef def = new FixtureDef();
						def.shape = polyShape;
						def.density = DENSITY;
						setExtraFixtureProperties(def, shape.getBodyDescriptor());
						mainSpriteBody.createFixture(def);
					}
				}
				else
				{
					Body independentBody = getBodyForLayerId(shape.getBodyDescriptor());
					if (independentBody == null)
					{
						independentBody = initBody(physicsWorld, shape);
						if (shape.getShapeBoundingRectangle() != null)
						{
							float x = shape.getShapeBoundingRectangle().x;
							float y = shape.getShapeBoundingRectangle().y;
							independentBody.setTransform(x, y, 0);
						}

						BodyDescriptor ud = (BodyDescriptor) independentBody.getUserData();
						ud.getFixtureIds().add(shape.getBodyDescriptor().getBodyId());

						PhysicsSprite independentBodySprite = null;
						if (shape.getSpriteTexture() != null)
						{
							Sprite texSprite = null;
							if (!shape.getBodyDescriptor().isMapBody())
							{
								texSprite = new Sprite(shape.getSpriteTexture());
								texSprite.setSize(texSprite.getWidth() / PIXEL_METER_RATIO, texSprite.getHeight() / PIXEL_METER_RATIO);
							}
							else
							{
								texSprite = new Sprite(shape.getSpriteTexture());
								texSprite.setOrigin(texSprite.getWidth() / 2, texSprite.getHeight() / 2);
								texSprite.setSize(texSprite.getWidth() / shape.getEditorScaleFactor().x,
										texSprite.getHeight() / shape.getEditorScaleFactor().y);
							}

							independentBodySprite = new PhysicsSprite(texSprite, independentBody);
						}
						else
						{
							independentBodySprite = new PhysicsSprite(new Sprite(), independentBody);
						}

						independentBodySprite.setOrigin(0, 0);
						independentBodySprite.spriteType = MAIN_SPRITE;
						independentBodySprite.spriteBoundingRectangle = shape.getShapeBoundingRectangle();
						subBodies.add(independentBodySprite);
					}
					else
					{
						BodyDescriptor ud = (BodyDescriptor) independentBody.getUserData();
						ud.getFixtureIds().add(shape.getBodyDescriptor().getBodyId());
						independentBody.setUserData(ud);

						PolygonShape polyShape = LevelEditor.getPolygonShape(shape.getWorldVertices());
						FixtureDef def = new FixtureDef();
						def.shape = polyShape;
						def.density = DENSITY;
						setExtraFixtureProperties(def, ud);
						independentBody.createFixture(polyShape, DENSITY);
					}
				}
			}
		}

		if (mainSpriteBody == null && subBodies.size() > 0)
		{
			mainSpriteBody = subBodies.get(0).getMainSpriteBody();
		}
		else
		{
			setOrigin(0, 0);
			spriteType = MAIN_SPRITE;
		}

		initJoints(spriteEditor);
		initSprite();
	}

	private Body getBodyForLayerId(BodyDescriptor bd)
	{
		Iterator<Body> bodyList = physicsWorld.getBodies();
		while (bodyList.hasNext())
		{
			Body body = (Body) bodyList.next();
			BodyDescriptor bodyUD = (BodyDescriptor) body.getUserData();
			if (bodyUD != null)
			{
				String bodyLayerId = bodyUD.getLayerId();
				String bodyResourcePath = bodyUD.getResourcePath();
				if (bodyLayerId.equals(bd.getLayerId()) && bodyResourcePath.equals(bd.getResourcePath()))
				{
					return body;
				}
			}
		}
		return null;
	}

	public void initJoints(LevelEditor lvEditor)
	{
		ArrayList<LevelEditorJoint> jointList = lvEditor.getJointList();
		for (int j = 0; j < jointList.size(); j++)
		{
			LevelEditorShape[] associatedBodies = jointList.get(j).getConnectedBodies();
			if (associatedBodies[0] == null || associatedBodies[1] == null)
			{
				try
				{
					throw new Exception("Associated bodies null: " + jointList.get(j).getBodyDescriptor().getBodyId());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			Iterator<Body> worldBodyList = physicsWorld.getBodies();
			Body body1 = null, body2 = null;
			while (worldBodyList.hasNext())
			{
				Body body = (Body) worldBodyList.next();
				BodyDescriptor bodyDescriptor = (BodyDescriptor) body.getUserData();
				if (bodyDescriptor != null)
				{
					ArrayList<String> possibleFixtureIds = bodyDescriptor.getFixtureIds();
					if (possibleFixtureIds != null && possibleFixtureIds.size() > 0)
					{
						for (int i = 0; i < possibleFixtureIds.size(); i++)
						{
							String bodyId = possibleFixtureIds.get(i);
							if (bodyId != null)
							{
								if (bodyId.equals(associatedBodies[0].getBodyDescriptor().getBodyId())
										&& bodyDescriptor.getResourcePath().equals(associatedBodies[0].getBodyDescriptor().getResourcePath()))
								{
									body1 = body;
								}
								else if (bodyId.equals(associatedBodies[1].getBodyDescriptor().getBodyId())
										&& bodyDescriptor.getResourcePath().equals(associatedBodies[1].getBodyDescriptor().getResourcePath()))
								{
									body2 = body;
								}
							}
						}
					}
					else
					{
						String bodyId = bodyDescriptor.getBodyId();
						if (bodyId != null)
						{
							if (bodyId.equals(associatedBodies[0].getBodyDescriptor().getBodyId())
									&& bodyDescriptor.getResourcePath().equals(associatedBodies[0].getBodyDescriptor().getResourcePath()))
							{
								body1 = body;
							}
							else if (bodyId.equals(associatedBodies[1].getBodyDescriptor().getBodyId())
									&& bodyDescriptor.getResourcePath().equals(associatedBodies[1].getBodyDescriptor().getResourcePath()))
							{
								body2 = body;
							}
						}
					}
				}
			}

			if (body1 == null || body2 == null)
			{
				if (body1 == null)
				{
					try
					{
						throw new Exception("Body 1 in joint: " + jointList.get(j).getBodyDescriptor().getBodyId() + " is null! Associated bodies: "
								+ associatedBodies[0].getBodyDescriptor().getBodyId() + " ; " + associatedBodies[1].getBodyDescriptor().getBodyId());
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					try
					{
						throw new Exception("Body 2 in joint: " + jointList.get(j).getBodyDescriptor().getBodyId() + " is null! Associated bodies: "
								+ associatedBodies[0].getBodyDescriptor().getBodyId() + " ; " + associatedBodies[1].getBodyDescriptor().getBodyId());
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}

			float localJointX = jointList.get(j).getShapeCenterX();
			float localJointY = jointList.get(j).getShapeCenterY();
			Vector2 anchor = new Vector2(localJointX, localJointY);

			String jointType = jointList.get(j).getJointType();
			String jointId = jointList.get(j).getBodyDescriptor().getBodyId();

			if (jointType.equals(LevelEditorJoint.TYPE_PRISMATIC_JOINT))
			{
				LevelEditorShape prismaticAxis = jointList.get(j).getPrismaticAxis();

				float startX = prismaticAxis.getWorldVertices().get(0).x;
				float startY = prismaticAxis.getWorldVertices().get(0).y;
				float endX = prismaticAxis.getWorldVertices().get(1).x;
				float endY = prismaticAxis.getWorldVertices().get(1).y;
				float dx = endX - startX;
				float dy = endY - startY;
				float hyp = (float) Math.sqrt(dx * dx + dy * dy);
				Vector2 axis = new Vector2(dx, dy);
				axis = axis.nor();

				PrismaticJointDef springDef = new PrismaticJointDef();
				springDef.bodyA = body1;
				springDef.bodyB = body2;
				springDef.localAxisA.set(axis);
				springDef.localAnchorA.set(localJointX, localJointY);
				springDef.localAnchorB.set(0, 0);
				springDef.lowerTranslation = -hyp;
				springDef.upperTranslation = hyp;
				springDef.enableLimit = true;
				setExtraJointProperties(springDef, jointList.get(j).getBodyDescriptor());

				PrismaticJoint prismJoint = (PrismaticJoint) physicsWorld.createJoint(springDef);
				this.jointList.add(new Tuple<Joint, String>(prismJoint, jointId));
			}
			else if (jointType.equals(LevelEditorJoint.TYPE_REVOLUTE_JOINT))
			{
				RevoluteJointDef def = new RevoluteJointDef();
				BodyDescriptor descriptor = (BodyDescriptor) body1.getUserData();
				BodyDescriptor descriptor2 = (BodyDescriptor) body2.getUserData();
				if (descriptor.getShapeType() == LevelEditorShape.ShapeType.TYPE_ELLIPSE)
				{
					def.initialize(body1, body2, body1.getWorldCenter());
				}
				else if (descriptor2.getShapeType() == LevelEditorShape.ShapeType.TYPE_ELLIPSE)
				{
					def.initialize(body1, body2, body2.getWorldCenter());
				}
				else
				{
					def.initialize(body1, body2, anchor);
				}

				def.enableLimit = false;
				def.enableMotor = true;
				setExtraJointProperties(def, jointList.get(j).getBodyDescriptor());
				RevoluteJoint rvj = (RevoluteJoint) physicsWorld.createJoint(def);
				this.jointList.add(new Tuple<Joint, String>(rvj, jointId));
			}
			else if (jointType.equals(LevelEditorJoint.TYPE_WELD_JOINT))
			{
				WeldJointDef wjdf = new WeldJointDef();
				wjdf.initialize(body1, body2, anchor);
				setExtraJointProperties(wjdf, jointList.get(j).getBodyDescriptor());

				WeldJoint wjd = (WeldJoint) physicsWorld.createJoint(wjdf);
				this.jointList.add(new Tuple<Joint, String>(wjd, jointId));
			}
			else if (jointType.equals(LevelEditorJoint.TYPE_DISTANCE_JOINT))
			{
				DistanceJointDef djdf = new DistanceJointDef();
				djdf.initialize(body1, body2, body1.getWorldCenter(), body2.getWorldCenter());
				setExtraJointProperties(djdf, jointList.get(j).getBodyDescriptor());

				DistanceJoint dJoint = (DistanceJoint) physicsWorld.createJoint(djdf);
				this.jointList.add(new Tuple<Joint, String>(dJoint, jointId));
			}
		}
	}

	public Rectangle getSpriteSuccessArea()
	{
		return spriteSuccessArea;
	}

	public void setSpriteSuccessArea(Rectangle spriteSuccessArea)
	{
		this.spriteSuccessArea = spriteSuccessArea;
	}

	// Should only activate triggers after all elements have been positioned
	public void createActivationTriggerForSprite(SpriteListener listener, PhysicsSprite triggerer)
	{
		this.activationTriggerer = triggerer;
		this.spriteListener = listener;

		// Must wait until coordinates are available for sprites
		if (canCreateActivationTriggers && getMainSpriteBody() != null && getMainSpriteBody().getUserData() != null)
		{
			addActivationTrigger(this);
			ArrayList<PhysicsSprite> subBodies = getSubBodies();
			for (int i = 0; i < subBodies.size(); i++)
			{
				addActivationTrigger(subBodies.get(i));
			}
		}
	}

	private void addActivationTrigger(PhysicsSprite sprite)
	{
		BodyDescriptor descriptor = (BodyDescriptor) sprite.getMainSpriteBody().getUserData();
		for (int y = 0; y < descriptor.getExtraProperties().size(); y++)
		{
			Tuple<String, String> extraProp = descriptor.getExtraProperties().get(y);
			if (extraProp.getKey().equals("activationTriggers"))
			{
				ActivationTriggerHolder holder = parseActivations(extraProp.getValue(), sprite);
				sprite.mActivationTriggers = holder;
				sprite.spriteListener = this.spriteListener;
				sprite.activationTriggerer = this.activationTriggerer;
			}
		}
	}

	private ActivationTriggerHolder parseActivations(String activations, PhysicsSprite sprite)
	{
		if (activations != null)
		{
			String[] activs = activations.replaceAll(" ", "").split(",");
			ActivationTriggerHolder holder = new ActivationTriggerHolder();

			for (int i = 0; i < activs.length; i++)
			{
				String indivActiv = activs[i];
				String key = "";

				float activationValue = -1.0f;
				if (indivActiv.contains("="))
				{
					activationValue = Float.parseFloat(indivActiv.split("=")[1]);
					key = indivActiv.split("=")[0];
				}
				else
				{
					key = indivActiv;
					if (key.equals(ActivationTriggerHolder.X_KEY))
					{
						activationValue = sprite.getX();
					}
					else if (key.equals(ActivationTriggerHolder.Y_KEY))
					{
						activationValue = sprite.getY();
					}
				}

				if (activationValue == -1.0f)
				{
					activationValue = ActivationTriggerHolder.NO_TRIGGER;
				}

				holder.addActivation(key, activationValue);
			}
			return holder;
		}

		return null;
	}

	public void destroySprite()
	{
		if (mainSpriteBody != null && physicsWorld != null)
		{
			physicsWorld.destroyBody(mainSpriteBody);
			mainSpriteBody = null;
			setTexture(null);
		}
	}

	protected FixtureDef createFixtureDef(LevelEditorShape shape)
	{
		Shape polyShape = null;
		if (shape.getWorldVertices() == null || shape.getType() == LevelEditorShape.ShapeType.TYPE_ELLIPSE)
		{
			polyShape = new CircleShape();
			polyShape.setRadius(shape.getRectWidth() / 2);
		}
		else
		{
			polyShape = (PolygonShape) LevelEditor.getPolygonShape(shape.getWorldVertices());
		}

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = polyShape;
		fixtureDef.density = DENSITY;
		fixtureDef.friction = FRICTION;
		fixtureDef.restitution = 0.5f;
		setExtraFixtureProperties(fixtureDef, shape.getBodyDescriptor());

		return fixtureDef;
	}

	protected BodyDef createBodyDef(LevelEditorShape shape)
	{
		BodyDef bodyDef = new BodyDef();
		BodyDescriptor bd = shape.getBodyDescriptor();
		setExtraBodyProperties(bodyDef, bd);
		if (shape.getBodyDescriptor().getBodyType() != null)
		{
			bodyDef.type = shape.getBodyDescriptor().getBodyType();
		}
		else
		{
			bodyDef.type = BodyType.DynamicBody;
		}
		return bodyDef;
	}

	public Vector2 getCenter()
	{
		float centerX = (getX() + getWidth() / 2);
		float centerY = (getY() + getHeight() / 2);
		return new Vector2(centerX, centerY);
	}

	public void centerBody(Body body)
	{
		body.setTransform((getX() + getWidth() / 2), (getY() - getHeight() / 2), 0);
	}

	@Override
	public void setPosition(float x, float y)
	{
		setX(x);
		setY(y);
	}

	public void translatePhysicsSprite(float destX, float destY)
	{
		float startX = 0.0f, startY = 0.0f;
		if (mainSpriteBody != null)
		{
			startX = mainSpriteBody.getPosition().x;
			startY = mainSpriteBody.getPosition().y;
			mainSpriteBody.setTransform(destX, destY, 0);
		}
		for (PhysicsSprite sprite : subBodies)
		{
			float desiredDx = startX - sprite.getMainSpriteBody().getPosition().x;
			float desiredDy = startY - sprite.getMainSpriteBody().getPosition().y;
			float newX = mainSpriteBody.getPosition().x - desiredDx;
			float newY = mainSpriteBody.getPosition().y - desiredDy;
			Body body = sprite.getMainSpriteBody();
			body.setTransform(newX, newY, 0);
		}
	}

	@Override
	// Angle is in radians
	public void rotate(float angle)
	{
		mainSpriteBody.setTransform(mainSpriteBody.getPosition(), angle);
		for (PhysicsSprite subSprite : subBodies)
		{
			subSprite.getMainSpriteBody().setTransform(subSprite.getMainSpriteBody().getPosition(), angle);
		}
	}

	public void setFailureBounds(SpriteListener listener, PointF start, PointF end)
	{
		this.spriteListener = listener;
		spriteFailureBounds = new Rectangle(start.x, start.y, end.x, end.y);
	}

	public void beginContact(Contact contact)
	{
		BodyDescriptor bodyDescriptor = (BodyDescriptor) contact.getFixtureA().getBody().getUserData();
		BodyDescriptor bodyDescriptor2 = (BodyDescriptor) contact.getFixtureB().getBody().getUserData();

		int groupIndex = bodyDescriptor.getGroupIndex();
		int groupIndex2 = bodyDescriptor2.getGroupIndex();
		boolean shouldDestruct = (bodyDescriptor.isSubDestructionBody() || bodyDescriptor2.isSubDestructionBody());

		if (shouldDestruct)
		{
			/*
			 * If no group indices have been set (0), then destroy the body. If they have been set, then they must be different for the body to be
			 * destroyed
			 */
			if (groupIndex == 0 && groupIndex2 == 0 || groupIndex != groupIndex2)
			{
				if (spriteListener != null)
				{
					spriteListener.spriteDestroyed(this);
				}
			}
		}
	}

	public void endContact(Contact contact)
	{

	}

	public void preSolve(Contact contact, Manifold oldManifold)
	{

	}

	public void postSolve(Contact contact, ContactImpulse impulse)
	{

	}

	public Joint getJointForId(String jointId)
	{
		ArrayList<Tuple<Joint, String>> jointList = getJointList();
		for (Tuple<Joint, String> joint : jointList)
		{
			if (joint.getValue().equals(jointId))
			{
				return joint.getKey();
			}
		}
		return null;
	}

	protected void setExtraJointProperties(JointDef jd, BodyDescriptor descriptor)
	{
		ArrayList<Tuple<String, String>> exProps = descriptor.getExtraProperties();
		for (int i = 0; i < exProps.size(); i++)
		{
			String name = exProps.get(i).getKey();
			String key = exProps.get(i).getValue();
			if (name.equals("collideConnected"))
			{
				jd.collideConnected = Boolean.parseBoolean(key);
			}
			else if (name.equals("enableLimit"))
			{
				((RevoluteJointDef) jd).enableLimit = Boolean.parseBoolean(key);
			}
			else if (name.equals("upperAngle"))
			{
				((RevoluteJointDef) jd).upperAngle = Float.parseFloat(key);
				if (!((RevoluteJointDef) jd).enableLimit)
				{
					((RevoluteJointDef) jd).enableLimit = true;
				}
			}
			else if (name.equals("lowerAngle"))
			{
				((RevoluteJointDef) jd).lowerAngle = Float.parseFloat(key);
				if (!((RevoluteJointDef) jd).enableLimit)
				{
					((RevoluteJointDef) jd).enableLimit = true;
				}
			}
			else if (name.equals("maxMotorTorque"))
			{
				((RevoluteJointDef) jd).maxMotorTorque = Float.parseFloat(key);
				((RevoluteJointDef) jd).enableMotor = true;
			}
			else if (name.equals("motorSpeed"))
			{
				((RevoluteJointDef) jd).motorSpeed = Float.parseFloat(key);
				((RevoluteJointDef) jd).enableMotor = true;
			}
			else if (name.equals("upperTranslation"))
			{
				((PrismaticJointDef) jd).upperTranslation = Float.parseFloat(key);
				((PrismaticJointDef) jd).enableLimit = true;
			}
			else if (name.equals("lowerTranslation"))
			{
				((PrismaticJointDef) jd).lowerTranslation = Float.parseFloat(key);
				((PrismaticJointDef) jd).enableLimit = true;
			}
		}
	}

	protected void setExtraBodyProperties(BodyDef bDef, BodyDescriptor descriptor)
	{
		ArrayList<Tuple<String, String>> exProps = descriptor.getExtraProperties();
		for (int i = 0; i < exProps.size(); i++)
		{
			String name = exProps.get(i).getKey();
			String key = exProps.get(i).getValue();
			if (name.equals("angularVelocity"))
			{
				bDef.angularVelocity = Float.parseFloat(key);
			}
			else if (name.equals("gravityScale"))
			{
				bDef.gravityScale = Float.parseFloat(key);
			}
		}
	}

	protected void setExtraFixtureProperties(FixtureDef fDef, BodyDescriptor descriptor)
	{
		ArrayList<Tuple<String, String>> exProps = descriptor.getExtraProperties();
		for (int i = 0; i < exProps.size(); i++)
		{
			String name = exProps.get(i).getKey();
			String value = exProps.get(i).getValue();

			if (name.equals("density"))
			{
				fDef.density = Float.parseFloat(value);
			}
			else if (name.equals("friction"))
			{
				fDef.friction = Float.parseFloat(value);
			}
			else if (name.equals("groupIndex"))
			{
				fDef.filter.groupIndex = (short) Integer.parseInt(value);
			}
			else if (name.equals("restitution"))
			{
				fDef.restitution = Float.parseFloat(value);
			}

		}
	}

	@Override
	public void setX(float x)
	{
		super.setX(x - getWidth() / 2);
	}

	@Override
	public void setY(float y)
	{
		super.setY(y - getHeight() / 2);
	}

	public float getVelocityX()
	{
		return getMainSpriteBody().getLinearVelocity().x;
	}

	public void setVelocityX(float velocityX)
	{
		this.getMainSpriteBody().setLinearVelocity(velocityX, getMainSpriteBody().getLinearVelocity().y);
	}

	public int getSpriteType()
	{
		return spriteType;
	}

	public void setSpriteType(int spriteType)
	{
		this.spriteType = spriteType;
	}

	public float getVelocityY()
	{
		return getMainSpriteBody().getLinearVelocity().y;
	}

	public void setVelocityY(float velocityY)
	{
		this.getMainSpriteBody().setLinearVelocity(getMainSpriteBody().getLinearVelocity().x, velocityY);
	}

	public World getPhysicsWorld()
	{
		return physicsWorld;
	}

	public LevelEditor getSpriteEditor()
	{
		return spriteEditor;
	}

	public void setSpriteEditor(LevelEditor spriteEditor)
	{
		this.spriteEditor = spriteEditor;
	}

	public void setPhysicsWorld(World physicsWorld)
	{
		this.physicsWorld = physicsWorld;
	}

	public Rectangle getSpriteBoundingRectangle()
	{
		return spriteBoundingRectangle;
	}

	public void setSpriteBoundingRectangle(Rectangle spriteBoundingRectangle)
	{
		this.spriteBoundingRectangle = spriteBoundingRectangle;
	}

	public Body getMainSpriteBody()
	{
		return mainSpriteBody;
	}

	public ArrayList<PhysicsSprite> getSubBodies()
	{
		return subBodies;
	}

	public void setSubBodies(ArrayList<PhysicsSprite> subBodies)
	{
		this.subBodies = subBodies;
	}

	public void setMainSpriteBody(Body mainSpriteBody)
	{
		this.mainSpriteBody = mainSpriteBody;
	}

	public ArrayList<Tuple<Joint, String>> getJointList()
	{
		return jointList;
	}

	public Rectangle getSpriteGameBounds()
	{
		return spriteFailureBounds;
	}

	public void setSpriteGameBounds(Rectangle spriteGameBounds)
	{
		this.spriteFailureBounds = spriteGameBounds;
	}

	public void setJointList(ArrayList<Tuple<Joint, String>> jointList)
	{
		this.jointList = jointList;
	}

	public SpriteListener getSpriteListener()
	{
		return spriteListener;
	}

	public void setSpriteListener(SpriteListener spriteListener)
	{
		this.spriteListener = spriteListener;
	}

}
