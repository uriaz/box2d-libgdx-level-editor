package com.minibit.sprites;

public class PointF
{
	public float x;
	public float y;

	public PointF()
	{
		this.x = 0.0f;
		this.y = 0.0f;
	}

	public PointF(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public PointF(PointF pointF)
	{
		this.x = pointF.x;
		this.y = pointF.y;
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString()
	{
		String pointFString = x + ", " + y;
		return pointFString;
	}
}
