package com.minibit.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.minibit.gameUtils.Tuple;

public class ParralaxBackground
{
	private TiledBackgroundSprite[] tiledBackgroundSprites;
	private Matrix4 parralaxBackgroundMatrix;
	private PointF screenDims;
	private float currentZoom;

	public static final float NO_SCROLL = 9873f; // If the background should not be moved at all,
													// even if the camera moves

	// Tuple<String, Float>[] backgrounds; String = background id, Float = background scroll speed
	public ParralaxBackground(Tuple<String, Float>[] backgrounds, float cameraZoom,
			PointF screenDims)
	{
		this.currentZoom = cameraZoom;
		this.screenDims = screenDims;
		createTiledBackgroundLevels(backgrounds);
	}

	public void createTiledBackgroundLevels(Tuple<String, Float>[] backgrounds)
	{
		tiledBackgroundSprites = new TiledBackgroundSprite[backgrounds.length];

		for (int i = 0; i < tiledBackgroundSprites.length; i++)
		{
			Texture bgt = new Texture(Gdx.files.internal(backgrounds[i].getKey()));
			tiledBackgroundSprites[i] = new TiledBackgroundSprite(bgt, this.currentZoom, screenDims);
			tiledBackgroundSprites[i].setScrollMult(backgrounds[i].getValue());
		}

		tiledBackgroundSprites[0].setY(screenDims.y - tiledBackgroundSprites[0].getHeight());

		int lastIndex = tiledBackgroundSprites.length - 1;
		tiledBackgroundSprites[lastIndex].setY(-tiledBackgroundSprites[lastIndex].getHeight() / 2);
	}

	public void zoomChanged(float newZoom)
	{
		// If factor < 1, zooming out, else zooming in
		float factor = currentZoom / newZoom;

		this.currentZoom = newZoom;
	}

	public void update(Camera cameraToFollow, PointF spriteStartLoc)
	{
		float cameraX = cameraToFollow.position.x;
		float cameraY = cameraToFollow.position.y;
		float dx = cameraX - spriteStartLoc.x;
		float dy = cameraY - spriteStartLoc.y;
		TiledBackgroundSprite frontMostBackground = tiledBackgroundSprites[tiledBackgroundSprites.length - 1];
		if (dy < 0)
		{
			if (frontMostBackground.getY() + frontMostBackground.getHeight() < frontMostBackground
					.getHeight())
			{
				frontMostBackground.updateY(-dy * frontMostBackground.getScrollMult());
			}
		}
		else
		{
			frontMostBackground.updateY(-dy * frontMostBackground.getScrollMult());
		}

		for (TiledBackgroundSprite background : tiledBackgroundSprites)
		{
			if (background.getScrollMult() == NO_SCROLL) // Then we do not want any movement
			{
				background.updateX(dx); // Shift it in the same direction as moving camera, so there
										// isn't any movement of background.
			}
			else
			{
				background.updateX(-dx * background.getScrollMult());
			}
		}
	}

	public void render(SpriteBatch batch)
	{
		batch.setProjectionMatrix(parralaxBackgroundMatrix);
		batch.begin();
		
		for (int i = 0; i < tiledBackgroundSprites.length; i++)
		{
			tiledBackgroundSprites[i].draw(batch);
		}
		
		batch.end();
	}

	public Matrix4 getParralaxBackgroundMatrix()
	{
		return parralaxBackgroundMatrix;
	}

	public void setParralaxBackgroundMatrix(Matrix4 parralaxBackgroundMatrix)
	{
		this.parralaxBackgroundMatrix = parralaxBackgroundMatrix;
	}
}
