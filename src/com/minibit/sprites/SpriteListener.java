package com.minibit.sprites;

public interface SpriteListener
{
	public void spriteOutOfBounds(PhysicsSprite sprite);

	public void spriteDestroyed(PhysicsSprite sprite);

	public void activationTriggered(PhysicsSprite spriteTriggered);

	// Sprite reached the success area defined by a rectangle
	public void spriteReachedSuccessArea(PhysicsSprite sprite);
}
