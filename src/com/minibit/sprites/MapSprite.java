package com.minibit.sprites;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.minibit.gameUtils.BodyDescriptor;
import com.minibit.gameUtils.LevelEditor;
import com.minibit.gameUtils.LevelEditorShape;
import com.minibit.gameUtils.Tuple;

public class MapSprite extends PhysicsSprite
{
	private float mapMinY;
	private Sprite pathTexture;
	private ArrayList<PointF> mapVertices = new ArrayList<PointF>();

	public MapSprite(String spriteImagePath, Sprite pathTexture, String spriteLvEditorPath, World physicsWorld)
	{
		super(spriteImagePath, spriteLvEditorPath, physicsWorld);
		mapMinY = getSpriteEditor().getMapMinY();
		this.pathTexture = pathTexture;
	}

	/**
	 * Creates a map sprite.
	 * 
	 * @param mapImage
	 *            Background image for map; may just be a sprite with a width / height.
	 * @param pathTexture
	 *            Texture to outline all paths on the map with. May be null.
	 * @param lvEditorPath
	 *            Path to .tmx file for map.
	 * @param physicsWorld
	 *            World to place map in.
	 */
	public MapSprite(Sprite mapImage, Sprite pathTexture, String lvEditorPath, World physicsWorld)
	{
		super(mapImage, lvEditorPath, physicsWorld);
		mapMinY = getSpriteEditor().getMapMinY();
		this.pathTexture = pathTexture;
	}

	/**
	 * The map scale factor is 1.0, 1.0 because it wouldn't make sense to scale a map up / down in size, while it does make sense to scale some
	 * sprites down / up.
	 * 
	 * @param input
	 * @return
	 */
	@Override
	public PointF getMeterScaleFactor(Sprite input)
	{
		return new PointF(1.0f, 1.0f);
	}

	// Simply add 3rd and 4th vertices to complete the box.
	@Override
	public void createSpriteBody()
	{
		super.createSpriteBody();
		for (LevelEditorShape shape : getSpriteEditor().getCollisionBoundingShapes())
		{
			if (shape.getType() == LevelEditorShape.ShapeType.TYPE_CHAIN_SHAPE)
			{
				generateBoxPathPoints(shape);
			}
		}
	}

	// Default in case the path texture is null
	private int defaultPathHeight = 2;

	private void generateBoxPathPoints(LevelEditorShape shape)
	{
		ArrayList<PointF> vertices = shape.getWorldVertices();
		int[] bezierPoints = shape.getBezierPoints();

		if (bezierPoints != null)
		{
			// Add the specified bezier points
			for (int i = 0; i < bezierPoints.length; i += 3)
			{
				int startIndex = bezierPoints[i];
				int controlIndex = bezierPoints[i + 1];
				int endIndex = bezierPoints[i + 2];
				PointF start = vertices.get(startIndex);
				PointF control = vertices.get(controlIndex);
				PointF end = vertices.get(endIndex);

				ArrayList<PointF> bPoints = generateBezierCurve(start.x, start.y, end.x, end.y, control.x, control.y);
				// Now that we have generated all these points, we need to increase the bezierPoint indices by the size of the array
				for (int j = 0; j < bezierPoints.length; j++)
				{
					bezierPoints[j] += bPoints.size() - 3; // Subtract 3 for the ones we are removing below
				}

				vertices.remove(endIndex);
				vertices.remove(controlIndex);
				vertices.remove(startIndex);

				vertices.addAll(startIndex, bPoints);
			}
		}
		for (int i = 0; i < vertices.size(); i++)
		{
			// Create the third point and fourth of the rectangle in clockwise direction.
			if (i + 1 < vertices.size())
			{
				// Create new arraylist so we don't modify the LevelEditorShape list
				ArrayList<PointF> rectangleVerts = new ArrayList<PointF>();
				PointF p1 = new PointF(vertices.get(i));
				PointF p2 = new PointF(vertices.get(i + 1));
				rectangleVerts.add(p1);
				rectangleVerts.add(p2);

				float dx = p2.x - p1.x;
				float dy = p2.y - p1.y;
				float pHeight = pathTexture != null ? pathTexture.getHeight() : defaultPathHeight;
				float theta = (float) Math.atan2(dy, dx);

				float turnThetaP3 = (float) (Math.PI / 2 + theta);
				float dTurnXP3 = (float) (Math.cos(turnThetaP3) * pHeight);
				float dTurnYP3 = (float) (Math.sin(turnThetaP3) * pHeight);
				PointF p3 = new PointF(p2.x - dTurnXP3, p2.y - dTurnYP3);
				rectangleVerts.add(p3);

				float turnThetaP4 = (float) (Math.PI - Math.PI / 2 - theta);
				float dTurnXP4 = (float) (Math.cos(turnThetaP4) * pHeight);
				float dTurnYP4 = (float) (Math.sin(turnThetaP4) * pHeight);
				PointF p4 = new PointF(p1.x + dTurnXP4, p1.y - dTurnYP4);
				rectangleVerts.add(p4);
				mapVertices.addAll(rectangleVerts);

				BodyDef staticBodyDef = new BodyDef();
				staticBodyDef.type = BodyType.StaticBody;
				setExtraBodyProperties(staticBodyDef, shape.getBodyDescriptor());

				Body body = getPhysicsWorld().createBody(staticBodyDef);
				body.setUserData(shape.getBodyDescriptor());

				ChainShape chainShape = new ChainShape();
				chainShape.createChain(LevelEditor.convertListToArray(rectangleVerts));

				FixtureDef def = new FixtureDef();
				def.shape = chainShape;
				setExtraFixtureProperties(def, shape.getBodyDescriptor());
				body.createFixture(def);

				chainShape.dispose();
				getSubBodies().add(new PhysicsSprite(new Sprite(), body));
			}
		}
	}

	public void renderPathTexture(SpriteBatch batch)
	{
		for (LevelEditorShape shape : getSpriteEditor().getCollisionBoundingShapes())
		{
			ArrayList<PointF> vertices = shape.getWorldVertices();
			if (shape.getType() == LevelEditorShape.ShapeType.TYPE_CHAIN_SHAPE)
			{
				for (int i = 0; i < vertices.size(); i++)
				{
					if (i + 1 < vertices.size())
					{
						float startX = vertices.get(i).x;
						float startY = vertices.get(i).y;
						float endX = vertices.get(i + 1).x;
						float endY = vertices.get(i + 1).y;
						float dx = endX - startX;
						float dy = endY - startY;
						float angle = (float) Math.atan2(dy, dx);

						// Add sub dx and dy to account for rotated height of sprite.
						float theta = (float) (Math.PI / 2 + angle);
						float pHeight = pathTexture != null ? pathTexture.getHeight() : defaultPathHeight;
						float dy2 = (float) (Math.sin(theta) * pHeight);
						float dx2 = (float) (Math.cos(theta) * pHeight);
						float psl = (float) Math.sqrt(dx * dx + dy * dy);

						if (pathTexture != null)
						{
							pathTexture.setSize(psl, pHeight);
							pathTexture.setOrigin(0, 0);
							pathTexture.setRotation((float) (angle * 180 / Math.PI));
							pathTexture.setPosition(startX - dx2, startY - dy2);
							pathTexture.draw(batch);
						}
					}
				}
			}
		}
	}

	public static float calculateAngle(PointF start, PointF end)
	{
		float dx = end.x - start.x;
		float dy = end.y - start.y;
		return (float) Math.atan2(dy, dx);
	}

	public static ArrayList<PointF> generateBezierCurve(float startX, float startY, float endX, float endY, float controlX, float controlY)
	{
		ArrayList<PointF> wirePath = new ArrayList<PointF>();
		float accuracyIncrement = 0.1f;
		for (float t = 0; t < 1.0f; t += accuracyIncrement)
		{
			float x = (float) (Math.pow(1 - t, 3) * startX + 3 * (1 - t) * (1 - t) * t * controlX + t * t * 3 * (1 - t) * controlX + Math.pow(t, 3)
					* endX);
			float y = (float) (Math.pow(1 - t, 3) * startY + 3 * (1 - t) * (1 - t) * t * controlY + t * t * 3 * (1 - t) * controlY + Math.pow(t, 3)
					* endY);
			PointF point = new PointF(x, y);
			wirePath.add(point);
		}
		return wirePath;
	}

	public void renderDebugPath(ShapeRenderer renderer)
	{
		renderer.begin(ShapeType.Line);
		for (int i = 0; i < mapVertices.size(); i += 4)
		{
			for (int j = 0; j < 3; j++)
			{
				if (i + j + 1 < mapVertices.size())
				{
					float startX = mapVertices.get(i + j).x;
					float startY = mapVertices.get(i + j).y;
					float endX = mapVertices.get(i + j + 1).x;
					float endY = mapVertices.get(i + j + 1).y;
					renderer.line(startX, startY, endX, endY);
				}
			}
		}
		renderer.end();
	}

	public float getMapMinY()
	{
		return mapMinY;
	}

	public void setMapMinY(float mapMinY)
	{
		this.mapMinY = mapMinY;
	}

}
