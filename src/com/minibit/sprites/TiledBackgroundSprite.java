package com.minibit.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TiledBackgroundSprite extends Sprite
{

	private Sprite[] repeatTextures;
	private PointF screenDims;
	private float scrollMult;
	private float tileStartX, tileStartY;

	public TiledBackgroundSprite(Texture backgroundTexture, float cameraZoom, PointF screenDims)
	{
		this.screenDims = screenDims;
		int repeatX = Math.round(screenDims.x * cameraZoom);
		float intervalX = screenDims.x / repeatX;
		repeatTextures = new Sprite[repeatX];
		for (int i = 0; i < repeatTextures.length; i++)
		{
			repeatTextures[i] = new Sprite(backgroundTexture);
			// Scale with respect to default 800 x 400 screen size.
			float yScale = 1.0f;
			if (screenDims.x > backgroundTexture.getWidth())
			{
				// For textures that're stretched horizontally to meet screen edges, scale upwards so they don't look distorted
				float scaleAmount = screenDims.x / backgroundTexture.getWidth();
				yScale = scaleAmount;
			}

			float width = Math.max(screenDims.x, backgroundTexture.getWidth());

			repeatTextures[i].setSize(width, backgroundTexture.getHeight() * yScale);
			repeatTextures[i].setX(i * intervalX);
		}

		if (repeatTextures.length > 1)
		{
			set(repeatTextures[0]);
		}
		setX(getX() - getWidth() / 2);
	}

	@Override
	public void setY(float y)
	{
		tileStartY = y;
		for (Sprite sprite : repeatTextures)
		{
			sprite.setY(y);
		}
	}

	@Override
	public void setX(float x)
	{
		tileStartX = x;
		int i = 0;
		if (repeatTextures.length > 0)
		{
			for (Sprite sprite : repeatTextures)
			{
				sprite.setX(x + i * getWidth());
				i++;
			}
		}
	}

	public void setPosition(float x, float y)
	{
		for (Sprite sprite : repeatTextures)
		{
			sprite.setX(x);
			sprite.setY(y);
		}
	}

	public void update(float dx, float dy)
	{
		setX(getX() - getWidth() / 2 + dx);
		if (dy != 0)
		{
			setY(getY() + dy);
		}
	}

	public void updateX(float dx)
	{
		setX(getX() - getWidth() / 2 + dx);
	}

	public void updateY(float dy)
	{
		if (dy != 0)
		{
			setY(getY() + dy);
		}
	}

	@Override
	public void draw(SpriteBatch spriteBatch)
	{
		for (int i = 0; i < repeatTextures.length; i++)
		{
			repeatTextures[i].draw(spriteBatch);
		}
	}

	public float getScrollMult()
	{
		return scrollMult;
	}

	public void setScrollMult(float scrollMult)
	{
		this.scrollMult = scrollMult;
	}

	public float getTileStartX()
	{
		return tileStartX;
	}

	public void setTileStartX(float tileStartX)
	{
		this.tileStartX = tileStartX;
	}

	public float getTileStartY()
	{
		return tileStartY;
	}

	public void setTileStartY(float tileStartY)
	{
		this.tileStartY = tileStartY;
	}
}