package com.minibit.box2dLevelEditor;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.minibit.sprites.PhysicsSprite;
import com.minibit.sprites.PointF;
import com.minibit.sprites.SpriteListener;

/**
 * @author Usama This class contains information only on the physics and game-play mechanisms of the game. It should not do any rendering of textures
 *         / sprites.
 */
public class BasePhysicsGame implements ApplicationListener, SpriteListener, ContactListener
{
	private String mapId = "";
	private PointF screenDims;
	private World physicsWorld;

	private ArrayList<PhysicsSprite> gameSprites = new ArrayList<PhysicsSprite>();
	private ArrayList<PhysicsSprite> gameEndTriggers = new ArrayList<PhysicsSprite>();

	private float worldGravity = -2f;
	private boolean isGamePaused = false;

	public static String IMAGE_RESOURCE_PATH = "Sprite Image Data";
	public static String LEVEL_DATA_RESOURCE_PATH = "LevelXml/";

	// Notify some object of broad game events
	public static interface BroadLevelGameListener
	{
		public void clearedLevel(int timeToComplete);
	}

	private BroadLevelGameListener mBroadLevelGameListener;

	public BasePhysicsGame(String mapId)
	{
		if (mapId != null && !mapId.equals(""))
		{
			this.mapId = mapId;
		}
	}

	public BasePhysicsGame(String mapId, BroadLevelGameListener listener)
	{
		if (mapId != null && !mapId.equals(""))
		{
			this.mapId = mapId;
		}

		mBroadLevelGameListener = listener;
	}

	@Override
	public void create()
	{
		Texture.setEnforcePotImages(false);
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		screenDims = new PointF(w, h);

		physicsWorld = new World(new Vector2(0, worldGravity), true);
		physicsWorld.setContactListener(this);
		physicsWorld.setAutoClearForces(false);
	}

	public void reinitGame()
	{

	}

	private static final int MAX_STEPS = 5;
	private static final float TIME_STEP = 1.0f / 60.0f;
	private float fixedTimeStepAccumulator = 0, fixedTimeStepRatio = 0;
	private long oldTime = 0;
	private static final int RESTART_GAME_DELAY = 750;

	@Override
	public void render()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		if (restartGameAfterCollision)
		{
			if (System.currentTimeMillis() - restartGameTimeStamp >= RESTART_GAME_DELAY)
			{
				restartGame();
				restartGameAfterCollision = false;
			}
		}
		else if (restartScheduled)
		{
			restartGame();
			restartScheduled = false;
		}

		if (!isGamePaused)
		{
			long currentTime = System.currentTimeMillis();
			if (oldTime == 0)
			{
				oldTime = currentTime;
			}
			long dt = (long) (currentTime - oldTime);
			oldTime = currentTime;
			fixedTimeStepAccumulator = fixedTimeStepAccumulator + (float) dt;
			int nSteps = (int) Math.floor(fixedTimeStepAccumulator / TIME_STEP);
			if (nSteps > 0)
			{
				fixedTimeStepAccumulator -= nSteps * TIME_STEP;
			}

			fixedTimeStepRatio = fixedTimeStepAccumulator / TIME_STEP;
			int clampedSteps = Math.min(nSteps, MAX_STEPS);
			for (int i = 0; i < clampedSteps; i++)
			{
				for (int j = 0; j < gameSprites.size(); j++)
				{
					gameSprites.get(j).resetSmoothStates();
					gameSprites.get(j).handleUserInput();
				}

				physicsWorld.step(TIME_STEP, 8, 1);
			}

			physicsWorld.clearForces();
			destroyScheduledBodies();

			for (int i = 0; i < gameSprites.size(); i++)
			{
				gameSprites.get(i).smoothStateUpdate(fixedTimeStepRatio);
			}
		}
	}

	protected void restartGame()
	{
		for (PhysicsSprite sprite : gameSprites)
		{
			if (sprite.getTexture() != null)
			{
				sprite.destroySprite();
				sprite = null;
			}
		}

		physicsWorld.setContactListener(null);
		gameSprites.clear();
		physicsWorld.dispose();
		physicsWorld = null;
		System.gc();

		physicsWorld = new World(new Vector2(0, worldGravity), true);
		physicsWorld.setContactListener(this);
		reinitGame();
	}

	/**
	 * Re-initialization of game can only happen from the render method, so schedule a restart to happen on the next loop of the render cycle
	 */
	private boolean restartScheduled = false;

	public void scheduleRestart()
	{
		restartScheduled = true;
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{

	}

	@Override
	public void dispose()
	{
		if (physicsWorld != null)
		{
			physicsWorld.dispose();
		}
	}

	/*
	 * Must destroy bodies only after finished looping through contact listeners.
	 */
	private ArrayList<PhysicsSprite> bodiesToDestroy = new ArrayList<PhysicsSprite>();
	private boolean restartGameAfterCollision = false;
	private long restartGameTimeStamp = 0L;

	public void destroyScheduledBodies()
	{
		if (!physicsWorld.isLocked())
		{
			for (int i = 0; i < bodiesToDestroy.size(); i++)
			{
				for (int j = 0; j < gameEndTriggers.size(); j++)
				{
					if (bodiesToDestroy.get(i).equals(gameEndTriggers.get(j)))
					{
						restartGameAfterCollision = true;
						restartGameTimeStamp = System.currentTimeMillis();
					}
				}

				bodiesToDestroy.get(i).destroySprite();
				bodiesToDestroy.remove(i);
				i--;
			}
		}
	}

	public void spriteOutOfBounds(PhysicsSprite sprite)
	{
		restartGame();
	}

	public void restartGameClicked()
	{
		restartGame();
	}

	@Override
	public void spriteReachedSuccessArea(PhysicsSprite sprite)
	{
	}

	@Override
	public void activationTriggered(PhysicsSprite spriteTriggered)
	{

	}

	@Override
	public void beginContact(Contact contact)
	{
		for (int i = 0; i < gameSprites.size(); i++)
		{
			gameSprites.get(i).beginContact(contact);
		}
	}

	@Override
	public void endContact(Contact contact)
	{

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold)
	{

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse)
	{

	}

	@Override
	public void spriteDestroyed(PhysicsSprite sprite)
	{
		bodiesToDestroy.add(sprite);
	}

	public float getWorldGravity()
	{
		return worldGravity;
	}

	public BroadLevelGameListener getBroadLevelGameListener()
	{
		return mBroadLevelGameListener;
	}

	public void setBroadLevelGameListener(BroadLevelGameListener BroadLevelGameListener)
	{
		this.mBroadLevelGameListener = BroadLevelGameListener;
	}

	public void setWorldGravity(float worldGravity)
	{
		this.worldGravity = worldGravity;
		getPhysicsWorld().setGravity(new Vector2(0.0f, worldGravity));
	}

	public ArrayList<PhysicsSprite> getGameEndTriggers()
	{
		return gameEndTriggers;
	}

	public void setGameEndTriggers(ArrayList<PhysicsSprite> gameEndTriggers)
	{
		this.gameEndTriggers = gameEndTriggers;
	}

	@Override
	public void resize(int width, int height)
	{

	}

	public String getMapId()
	{
		return mapId;
	}

	public void setMapId(String mapId)
	{
		this.mapId = mapId;
	}

	public PointF getScreenDims()
	{
		return screenDims;
	}

	public void setScreenDims(PointF screenDims)
	{
		this.screenDims = screenDims;
	}

	public World getPhysicsWorld()
	{
		return physicsWorld;
	}

	public void setPhysicsWorld(World physicsWorld)
	{
		this.physicsWorld = physicsWorld;
	}

	public ArrayList<PhysicsSprite> getGameSprites()
	{
		return gameSprites;
	}

	public void setGameSprites(ArrayList<PhysicsSprite> gameSprites)
	{
		this.gameSprites = gameSprites;
	}

	public boolean isGamePaused()
	{
		return isGamePaused;
	}

	public void setGamePaused(boolean isGamePaused)
	{
		this.isGamePaused = isGamePaused;
	}

	public ArrayList<PhysicsSprite> getBodiesToDestroy()
	{
		return bodiesToDestroy;
	}

	public void setBodiesToDestroy(ArrayList<PhysicsSprite> bodiesToDestroy)
	{
		this.bodiesToDestroy = bodiesToDestroy;
	}
}
